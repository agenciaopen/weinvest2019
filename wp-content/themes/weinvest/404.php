<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

   <div class="page-interna sucesso">
    <section class="vitrine-interna">
        <div class="grid-container">
            <div class="content-vitrine cf">

                <div class="grid-30 content_sucesso">
                     <img src="<?php bloginfo('template_url')?>/build/imgs/erro404.jpg'" alt="">
                    <h1 class="title">
                       
                       Página 404
                    </h1>
                    <p>Página não encontrada</p>
                    <a href="<?= get_site_url(); ?>" title="Voltar para a página inicial" class="voltar">Voltar para a página inicial</a>
                </div>




            </div>
        </div>

    </section>
   
</div>

<?php get_footer(); ?>