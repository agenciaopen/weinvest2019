﻿var url = window.location.protocol + "//" + window.location.host;

var viewport = {
    w: $(window).width(),
    h: $(window).height()
};

$(window).resize(function () {
    viewport = {
        w: $(window).width(),
        h: $(window).height()
    };
});

var isMobile = function () {
    return (viewport.w < 768); 
};

var isTablet = function () {
    return (viewport.w >= 768 && viewport.w <= 1024);
};

var isDesktop = function () { 
    return (viewport.w > 1024);
};

function msieVersion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0) /* If Internet Explorer, return version number */
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    else /* If another browser, return 0 */
        return 0;
}
function ie() {
    var ua = window.navigator.userAgent,
      msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    return false;
}

function dataBgs() {
    //BG DESCKTOP
    $("[data-bg]").each(function () {
        if (isDesktop()) {
            $(this).css("background-image", "url('" + $(this).attr('data-bg') + "')");
        }
    });
    
    //BG MOBILE
    $("[data-bg-mobile]").each(function () {

        if (isMobile()) {
            $(this).css("background-image", "url('" + $(this).attr('data-bg-mobile') + "')");
        }

    });

    //BG COR
    $("[data-cor]").each(function () {
        $(this).css({
            "background": "rgba(" + $(this).attr('data-cor') + ",0.8)"
        });
        if (DetectIE() == '8') {
            $(this).css({
                "background": "rgb(" + $(this).attr('data-cor') + ")"
            });
        }

    });

}

function ancora(){
    var off;
    var $doc = $('html, body');
    $('.ancora').click(function() {
        
        off = $(this).data('ancora') ? $(this).data('ancora') : 0;

        $doc.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - off
        }, 800);
        return false;
    });
} 

function validaForm(btn) {
    /*validacao submit*/
    $('.' + btn).click(function (e) {
        e.preventDefault();
        
        var form = $(this).parents('form');
        var erro = 0;

        $('.formulario').addClass('validacao');

        $(this).parents(".formulario").find(".obrigatorio").each(function () {
            erro += validacaoCampos($(this));
        });

        if (erro == 0) {
            //Envia o form
            form.submit();
            
        } else {
            
            $('.alert').fadeIn();
            setTimeout(function(){ $('.alert').fadeOut(); }, 5000);
            return false;
        }

    });
}

function validacaoCampos(campo) {
    var valor = campo.val();
    var select = campo.val();
    var erro = 0;

    if (valor == "" || valor == "0" && campo.hasClass('obrigatorio')) {
        erro++;
    } else {
        if (campo.hasClass('email') && !emailCarac.exec(valor)) {
            erro++;
        }
    }

    if (select == "" || select == "0" || select == "CIDADE" || select == "SELECIONE O ESTADO" || select == "RELAÇÃO COM O TERRENO" || select == "Cidade" || select == "Estado" || select == "cidade" || select == "estado"  && campo.hasClass('obrigatorio')) {
        erro++;
        //alert(select);
    }

    if (erro > 0) {
        campo.parent().addClass('error').removeClass('ok');
        return 1;
    } else if (erro == 0 && valor != "") {
        campo.parent().addClass('ok').removeClass('error');  
        return 0;
    } else if (erro == 0 && valor == "") {
        return 0;
    }
}
var emailCarac = /^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*).*([a-zA-Z]{1,})+$/;

function video() {
    var boxVideo = $(".youtube_video");

    boxVideo.each(function () {
        var idVideo = $(this).attr('href');
        var str = idVideo;
        var res = str.replace("https://www.youtube.com/embed/", "");

        var urlImage = "https://img.youtube.com/vi/" + res + "/hqdefault.jpg";
        var img = new Image();
        img.src = urlImage;
        img.alt = $(this).attr('title');
        $(this).find('.img').append(img);

    });

}

function fancyIframe() {
    $(".FancyIframe").fancybox({ 
        openEffect: 'elastic',
        closeEffect: 'elastic',
        autoSize: true,
        type: 'iframe',
        iframe: {
            preload: false // fixes issue with iframe and IE
        }
    });
}

function fancyInline() {
    $(".FacyInline").fancybox({
        autoSize: false,
        maxWidth: '400px',
        height: 'auto',
        hideOnContentClick: true
    });
}


function check() {
    $('.check').click(function () {
        $(this).toggleClass('before-check');

        if ($(this).find('input').prop("checked")) {
            $(this).find('input').prop("checked", false);
            $(this).find('input').val(false);
        } else {
            $(this).find('input').prop("checked", true);
            $(this).find('input').val(true);
        }
    });
}

function radio() {
    $('.radio').click(function () {
        $('.radio').removeClass('before-check');
        $(this).toggleClass('before-check');
        $(this).find('input').prop("checked", true);
    });
}

function SliderVitrinePrincipal() {
    $('.vitrine-principal').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
		autoplay: true,
  		autoplaySpeed: 6000,
		pauseOnHover: false
    });
}

function sliderNews() {
    $('.slider-news').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 3,
                    dots: true
                }
            },{
                breakpoint:767,
                settings: { 
                    slidesToShow: 1,
                    dots: true
                } 
            }
        ]
    });
}

function sliderImprensa() {
  $('.lista-news-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          dots: true
        }
      }, {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          dots: true
        }
      }
    ]
  });
}

function SliderVideosHome() {
    $('.slider-videos-home').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    dots: true
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            }
        ]
    });
}

function SliderPadrao() {
    $('.slider-padrao').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true
    });
}

function dot() { 
    $('.dot').dotdotdot({
        ellipsis: '... ',
        wrap: 'word',
        fallbackToLetter: true,
        lastCharacter: {
            remove: [' ', ',', ';', '.', '!', '?'],
            noEllipsis: []
        }
    });
}

function dotDestroy(dot) {
    var $xmpl = $(dot);
    $xmpl.dotdotdot({})
    var api = $xmpl.data('dotdotdot');
    if ($xmpl.hasClass('ddd-truncated')) {
        api.restore();
    }
}

function modalLocalizacao() {
  $('#modalLocalizacao').fadeIn();
}

 $(document).ready(function() { 

    var ie = msieVersion() ? msieVersion() : ""; 
    if (ie) {
        $("html").addClass("ie" + ie); 
    }

    $("a[href='#']").click(function (e) {
        e.preventDefault();
    });
      
    $("[data-bg]").each(function () {
        $(this).css("background-image", "url('" + $(this).data('bg') + "')");  
        $(this).css("background-repeat","no-repeat");
    });

     if (typeof ($.fn.mask) == "function") {
        $("input[data-mask='cep']").mask("99.999-999"); 
        $(".telefone").mask("(99) 99999999");
        $(".celular").mask("(99) 99999999?9");
        $("input[data-mask='numbers']").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $("input[data-mask='telefone']").focusout(function () {
            var phone, element;
            element = $(this);
            element.unmask();
            phone = element.val().replace(/\D/g, '');
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9"); 
            }
        }).trigger('focusout');
     } 
    
});



 