(function () {
    'use strict';

    if ($('.pg-home').length > 0) {
        $(document).ready(function () {

            if ( (isTablet()) || (isMobile())) {

                $('.slider-post-sec').slick({
                    dots: false,
                    arrows: false,
                    infinite: true,
                    speed: 300,
                    variableWidth: true,
                    slidesToShow: 1
                });
            }



            $('.list-post-cat').slick({
                dots: false,
                arrows: true,
                infinite: false,
                speed: 300,
                slidesToShow: 2,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            variableWidth: true
                        }
                    }]

            });

            $('#botao').click(function (event) {
                if (
                    $('.toggle > input').is(':checked') &&
                    !$(event.target).parents('.toggle').is('.toggle')
                ) {
                    $('.toggle > input').prop('checked', false);
                }
            })


            $('#menu-cat li a').click(function () {
                var nomeClass = $(this).attr('class');
                $('#conteudo-menu-cat .list-post-cat').addClass('bx-none');
                $('.toggle > input').prop('checked', false);
                $('#conteudo-menu-cat #' + nomeClass).removeClass('bx-none');
                $('.list-post-cat').slick('setPosition');

            })


        });


    }
})();
