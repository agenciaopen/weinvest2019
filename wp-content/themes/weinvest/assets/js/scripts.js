(function () {
    'use strict';
    $(document).ready(function () {
        var $btn = $('.btn-menu');
        $btn.click(function () {
            $('body').toggleClass('show');
        })

        $('.list-post-cat-single').slick({
            dots: false,
                arrows: true,
                infinite: false,
                speed: 300,
                slidesToShow:2,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow:1,
                             variableWidth: true
                        }
                    }]

           
        });
        
        if ( (isTablet()) || (isMobile())) {

                $('.single-post-cat').slick({
                    dots: false,
                    arrows: true,
                    infinite: true,
                    speed: 300,
                    variableWidth: true,
                    slidesToShow: 1
                });
            }



    });


})();
