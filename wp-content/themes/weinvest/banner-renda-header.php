<section class="banner cf">

    <?php
        $banner = new WP_Query( 
            array(

                'post_type'      => 'banners'
            )
        );

        while ( $banner->have_posts() ) : $banner->the_post(); 

            $imageBanner = get_field('imagem_desk');
            $size = 'full';
            if( !empty($imageBanner) ): 

            ?>
            
            <img src="<?php echo $imageBanner['url']; ?>" alt="" />

        <?php endif; ?>
    <?php endwhile; ?>
</section>