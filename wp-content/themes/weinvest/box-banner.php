<section class="box-banner cf">
    <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
        <section class="banner cf">
            <?php
            $bannerm = new WP_Query(
                array(
                    'post_type'      => 'banners'
                )
            );

            while ($bannerm->have_posts()) : $bannerm->the_post();

                $imageBannerm = get_field('imagem-desk');
                $size = 'full';
                if (!empty($imageBannerm)) : ?>
                    <a href="<?= get_field('link'); ?>">
                        <img src="<?= $imageBannerm['url']; ?>" alt="" />
                    </a>
                <?php endif; ?>
            <?php endwhile; ?>
        </section>
    </div>

    <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
        <section class="banner cf">
            <?php
            $bannerm = new WP_Query(
                array(
                    'post_type'      => 'banners'
                )
            );

            while ($bannerm->have_posts()) : $bannerm->the_post();
                $imageBannerm = get_field('imagem_celular');
                $size = 'full';
                
                if (!empty($imageBannerm)) : ?>
                    <a href="<?= get_field('link'); ?>">
                        <img src="<?= $imageBannerm['url']; ?>" alt="" />
                    </a>
                <?php endif; ?>
            <?php endwhile; ?>
        </section>
    </div>
</section>