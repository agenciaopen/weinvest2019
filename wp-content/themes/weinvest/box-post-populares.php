<section class="cats cat-single cf hide-on-mobile">
    <div class="grid-container">
        <div class="s-cat cf">
            <div class="mobile-grid-100 grid-parent">
                <h2>Posts populares:</h2>
            </div>

        </div>
    </div>


    <div class="list-post-cat list-post-cat-single posts-populares">
        <?php // WP_Query arguments
            $args2 = array(
                'posts_per_page'         => '4',
                'meta_query'	=> array(
                    array(
                        'key'	 	=> 'post_popular',
                        'compare' 	=> '=',
                        'value'	  	=> true
                    )
                ),
            );

            // The Query
            $vitrine2 = new WP_Query( $args2 ); 

            if ( $vitrine2->have_posts() ) : 
                while ( $vitrine2->have_posts() ) : $vitrine2->the_post(); 
                    
                    $category = get_the_category();
                    $catParent = $category[0]->parent;

                    $catName = "";
                    $catSlug = "";

                    if( $catParent > 0){
                        $catPai = get_category( $catParent );
                        $catId = $catPai->cat_ID;
                        
                        $catName = $catPai->name;
                        $catSlug = $catPai->slug;
                    }else{
                        $catId   = $category[0]->cat_ID;
                        $catName = $category->name;
                        $catSlug = $category->slug;
                    }
                ?>
                    <div>
                        <div class="box-posts setCookie" data-cat="<?= $catId; ?>">
                            <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>)">
                                    <figcaption itemprop="caption description" class="caption">
                                        <span class="post-cat <?= $catSlug;?>">
                                            <?= $catName;?></span>
                                    </figcaption>
                                </figure>
                                <h2 class="<?= $catSlug; ?>">
                                <?= title_limite(60); ?>
                                </h2>
                                <span class="data">
                                    <?= get_the_date(); ?></span>
                                <p>
                                    <?php wp_limit_post(150,'...',true);?>
                                </p>
                            </a>
                        </div>
                    </div>

            <?php endwhile; ?>
        <?php endif; ?>
    </div>

</section>
