var url = window.location.protocol + "//" + window.location.host;

var viewport = {
    w: $(window).width(),
    h: $(window).height()
};

$(window).resize(function () {
    viewport = {
        w: $(window).width(),
        h: $(window).height()
    };
});

var isMobile = function () {
    return (viewport.w < 768); 
};

var isTablet = function () {
    return (viewport.w >= 768 && viewport.w <= 1024);
};

var isDesktop = function () { 
    return (viewport.w > 1024);
};

function msieVersion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0) /* If Internet Explorer, return version number */
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    else /* If another browser, return 0 */
        return 0;
}
function ie() {
    var ua = window.navigator.userAgent,
      msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    return false;
}

function dataBgs() {
    //BG DESCKTOP
    $("[data-bg]").each(function () {
        if (isDesktop()) {
            $(this).css("background-image", "url('" + $(this).attr('data-bg') + "')");
        }
    });
    
    //BG MOBILE
    $("[data-bg-mobile]").each(function () {

        if (isMobile()) {
            $(this).css("background-image", "url('" + $(this).attr('data-bg-mobile') + "')");
        }

    });

    //BG COR
    $("[data-cor]").each(function () {
        $(this).css({
            "background": "rgba(" + $(this).attr('data-cor') + ",0.8)"
        });
        if (DetectIE() == '8') {
            $(this).css({
                "background": "rgb(" + $(this).attr('data-cor') + ")"
            });
        }

    });

}

function ancora(){
    var off;
    var $doc = $('html, body');
    $('.ancora').click(function() {
        
        off = $(this).data('ancora') ? $(this).data('ancora') : 0;

        $doc.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - off
        }, 800);
        return false;
    });
} 

function loadIn(){
    $('.loading').fadeIn();
}

function loadOut(){
    $('.loading').fadeOut();
}
(function () {
    'use strict';

    if ($('.pg-home').length > 0) {
        $(document).ready(function () {
            Cookies.remove('categoria');

            if ( (isTablet()) || (isMobile())) {
                $('.slider-post-sec').slick({
                    dots: false,
                    arrows: false,
                    infinite: true,
                    speed: 300,
                    variableWidth: true,
                    slidesToShow: 1
                });
            }

            $('.list-post-cat').slick({
                dots: false,
                arrows: true,
                infinite: false,
                speed: 300,
                slidesToShow: 2,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            variableWidth: true
                        }
                    }]
            });

            $('#botao').click(function (event) {
                if ( $('.toggle > input').is(':checked') && !$(event.target).parents('.toggle').is('.toggle') ) {
                    $('.toggle > input').prop('checked', false);
                }
            });

            $('#menu-cat li a').click(function () {
                var nomeClass = $(this).attr('class');
                $('#conteudo-menu-cat .list-post-cat').addClass('bx-none');
                $('.toggle > input').prop('checked', false);
                $('#conteudo-menu-cat #' + nomeClass).removeClass('bx-none');
                $('.list-post-cat').slick('setPosition');

                $('.name-category').html($(this).html());
                $('#botao').html($(this).html());
            });

            $('.setCookie').on('click', function () {  
                Cookies.set('categoria', $(this).data('cat') );
            });
        });
    }
})();

(function () {
    'use strict';

    if ($('.pg-single').length > 0) {
        $(document).ready(function () {
            
            $('.setCookie').on('click', function () {  
                Cookies.set('categoria', $(this).data('cat') );
            });

            var cookie = Cookies.get('categoria');
            console.log(cookie);
        });
    }
})();
(function () {
    'use strict';
    $(document).ready(function () {
        var ie = msieVersion() ? msieVersion() : "";
        if (ie) {
            $("html").addClass("ie" + ie);
        }

        $("a[href='#']").click(function (e) {
            e.preventDefault();
        });

        document.addEventListener( 'wpcf7mailsent', function( event ) {
            location = 'https://weinvest.com.br/sucesso/';
        }, false );        

        $("[data-bg]").each(function () {
            $(this).css("background-image", "url('" + $(this).data('bg') + "')");
            $(this).css("background-repeat", "no-repeat");
        });

        if (typeof ($.fn.mask) == "function") {
            $("input[data-mask='cep']").mask("99.999-999");
            $(".telefone").mask("(99) 999999999");
           
             $('.dinheiro').mask('#.##0,00', {reverse: true});
            
            $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
            $(".numbers").mask("00000000");
            $(".celular").focusout(function () {
                var phone, element;
                element = $(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if (phone.length > 10) {
                    element.mask("(99) 99999-99999");
                } else {
                    element.mask("(99) 9999-999999");
                }
            }).trigger('focusout');
        }

        var $btn = $('.btn-menu');
        $btn.click(function () {
            $('body').toggleClass('show');
        });

        var $btnBusca = $('.abrir-busca');
        $btnBusca.on('click', function () {
            $('.busca').fadeIn();
            $('.fecha-busca').click(function (e) {
                e.preventDefault();
                $('.busca').fadeOut();
            });
        });

        $('.list-post-cat-single').slick({
            dots: false,
            arrows: true,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true
                    }
                }
            ]
        });

        if ((isTablet()) || (isMobile())) {
            $('.single-post-cat').slick({
                dots: false,
                arrows: true,
                infinite: false,
                speed: 300,
                variableWidth: true,
                slidesToShow: 1
            });
        }
    });

    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= $('.h-top').height()) {
            $('.header').addClass('fixed-menu');
            $('.logo-fixed').css('display', 'block');
        } else {
            $('.header').removeClass('fixed-menu');
            $('.logo-fixed').css('display', 'none');
        }
    });


    if (~window.location.href.indexOf("acoes")) {
        $('.nav-menu li.acoes').addClass('active');
    } else if (~window.location.href.indexOf("como-investir")) {
        $('.nav-menu li.como-investir').addClass('active');
    } else if (~window.location.href.indexOf("renda-fixa")) {
        $('.nav-menu li.renda-fixa').addClass('active');
    } else if (~window.location.href.indexOf("tesouro-direto")) {
        $('.nav-menu li.tesouro-direto').addClass('active');
    } else if (~window.location.href.indexOf("fundos-de-investimentos")) {
        $('.nav-menu li.fundos-de-investimentos').addClass('active');
    }
})();

$(function() {
		var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;

				var links = this.el.find('.acc');
				links.on('click', {
						el: this.el,
						multiple: this.multiple
				}, this.dropdown)
		}

		Accordion.prototype.dropdown = function(e) {
				var $el = e.data.el;
				$this = $(this),
						$next = $this.next();

				$next.slideToggle(100);
                $('.acc').removeClass('open');
				$this.toggleClass('open');
                $('.accordion-content').not($next).slideUp().removeClass('open');

				
		}
		var accordion = new Accordion($('.accordion-container'), false);
});