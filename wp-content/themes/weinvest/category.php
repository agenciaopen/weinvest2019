<?php

get_header();
?>
<div class="page-interna page-list pg-single cf">
    <div class="grid-container grid-parent">
        <div class="tablet-grid-60 tablet-prefix-20 tablet-suffix-20 grid-70 prefix-15 suffix-30 hide-on-desktop">
           <section class="box-banner cf">
                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                            'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();

                                        $imageBannerm = get_field('imagem-desk');
                                        $size = 'full';
                                        if (!empty($imageBannerm)) : ?>
                                            <a href="<?= get_field('link'); ?>">
                                                <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                            </a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>

                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                          'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();
                                        $imageBannerm = get_field('imagem_celular');
                                        $size = 'full';

                                        if (!empty($imageBannerm)) : ?>
                                            <a href="<?= get_field('link'); ?>">
                                                <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                            </a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>
                        </section>
        </div>
        <!-- Main  -->
        <div class="tablet-grid-70 grid-parent grid-70 suffix-5">
            <section class="brands">

                <div class="grid-container">
                    <?php
                    if (function_exists('yoast_breadcrumb')) {
                        yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                    }
                    ?>
                </div>

            </section>
            <section class="cats  cf">
                <section class="menu-posts cf">
                    <div class="grid-container">

                        <ul>
                            <li>
                                <span class="align-center">FILTROS </span>
                            </li>
                            <?php
                            $cat = get_query_var('cat');
                            $the_cat = get_category($cat);
                            $catPai = $the_cat->parent;

                            if ($catPai > 0) {
                                $categorias = get_categories(array(
                                    'orderby'  => 'name',
                                    'child_of' => $catPai
                                ));
                            } else {
                                $categorias = get_categories(array(
                                    'orderby'  => 'name',
                                    'child_of' => $cat
                                ));
                            }

                            foreach ($categorias as $categoria) { ?>
                                <li class="<?php if ($categoria->cat_ID == $cat) echo "ativo"; ?>">
                                    <a href="<?= get_category_link($categoria->cat_ID); ?>">
                                        <?= $categoria->cat_name; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </section>

                <section class="list-posts cf">
                    <?php
                        $cat = get_query_var('cat');
                        $category = get_category($cat);
                        
                        $catId = $cat;

                        $catParent = $category->parent;
                        $catName = "";
                        $catSlug = "";

                        if( $catParent > 0){
                            $catPai = get_category( $catParent );
                            $catId = $catPai->cat_ID;
                            $catName = $catPai->name;
                            $catSlug = $catPai->slug;
                        }else{
                            $catName = $category->name;
                            $catSlug = $category->slug;
                        }

                        $args = array(
                            'cat' => $cat,
                            'posts_per_page' => 20
                        );
                        $the_query = new WP_Query($args);

                        // The Loop
                        if ($the_query->have_posts()) {
                            while ($the_query->have_posts()) : $the_query->the_post();
                                ?>

                                <div class="tablet-grid-50 grid-50 the_post">
                                    <div class="box-list-cats box-posts setCookie" data-cat="<?= $catId; ?>">
                                        <a href="<?php the_permalink(); ?>" title="">
                                            <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">

                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName; ?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                                <?= title_limite(270); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>


                            <?php endwhile;
                        }

                        wp_reset_postdata();
                    ?>
                </section>

            </section>
            <?php require 'box-post-populares.php'; ?>
        </div>


        <!-- Main  -->

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>

</div>
<?php
get_footer(); ?>