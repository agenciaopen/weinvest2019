<?php
/**
 * Aqui vamos garantir que os comentários só vão aparecer em posts, páginas ou
 * anexos. Além disso, também bloqueamos os comentários para páginas com senha,
 * eles só vão aparecer se o usuário digitar a senha.
 */
    if ( post_password_required() || !is_singular() ) {
     return;
    }
?>

<div class="comentarios_topo cf">
    <div class="grid-100 prefix-100 tablet-grid-100">
        <h2 class="h">
            Deixe seu comentário
        </h2>
    </div>

    
</div>


<div class="comentarios_list cf">
    <?php if ( have_comments() ) : ?>
    <ul>
        <?php wp_list_comments('type=comment&callback=mytheme_comment'); ?>
    </ul>
    <?php if ($wp_query->max_num_pages > 1) : ?>
        <div class="pagination">
        <ul>
            <li class="older"><?php previous_comments_link('Anteriores'); ?></li>
            <li class="newer"><?php next_comments_link('Novos'); ?></li>
        </ul>
    </div>
    <?php endif; ?>
 
    <?php endif; ?>
</div>

<div class="comentarios_form cf" id="respond">
    <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
        <div class="grid-50 tablet-grid-50">
            <div class="row rounded">
                <i class="fa fa-user-o"></i>
                <input type="text" id="author" name="author" placeholder="Insira seu nome aqui" class="obrigatorio rounded">
            </div>
        </div>
        <div class="grid-50 tablet-grid-50">
            <div class="row rounded">
                <i class="fa fa-envelope-o"></i>
                <input type="text" id="email" name="email" placeholder="Insira seu e-mail aqui" class="obrigatorio rounded">
            </div>
        </div>

        <div class="grid-100">
            <textarea name="comment" id="comment" cols="30" rows="7" placeholder="Deixe aqui sua mensagem"></textarea>
        </div>

        <div class="grid-50">
            <!-- Captcha -->
        </div>

        <div class="grid-50">
            <div class="btn">
                <button type="submit"> ENVIAR COMENTÁRIO </button>
            </div>
        </div>

        <?php comment_id_fields(); ?>
        <?php do_action('comment_form', $post->ID); ?>
        
    </form>

</div>
