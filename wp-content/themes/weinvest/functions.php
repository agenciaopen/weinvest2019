<?php


/* Thumbnails */
add_theme_support('post-thumbnails');
add_theme_support( 'title-tag' );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}



// function enable_comments_for_all(){
//     global $wpdb;
//     $wpdb->query( $wpdb->prepare("UPDATE $wpdb->posts SET comment_status = 'open'")); // Enable comments
//     $wpdb->query( $wpdb->prepare("UPDATE $wpdb->posts SET ping_status = 'open'")); // Enable trackbacks
// } enable_comments_for_all();

add_filter('comment_post_redirect', 'redirect_after_comment');
function redirect_after_comment($location)
{
return $_SERVER["HTTP_REFERER"];
}

function wpdocs_custom_excerpt_length( $length ) {
    return '5';
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
function custom_excerpt_more( $more ) {
    return '';//you can change this to whatever you want
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 * Enables the Excerpt meta box in Page edit screen.
 */
function wpcodex_add_excerpt_support_for_pages() {
	add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_pages' );


function favicon4admin() {
echo '<link rel="Shortcut Icon" type="image/x-icon" href="' . get_bloginfo('wpurl') . '/wp-content/themes/weinvest/favicon.ico" />';
}
add_action( 'admin_head', 'favicon4admin' );



function dw_get_category() {
    global $post;
    $categories = get_the_category($post->ID);
        if ( $categories ) {
           foreach ($categories as $category) {
               $dw_category_slug = $category->slug;
               $dw_category_name = $category->name;
           }
        } else {
            $dw_category_slug = 'utopia';
        }         
    $dw_category = array('name' => $dw_category_name, 'slug' =>    $dw_category_slug);
    return $dw_category;     
    } 


// Vitrines
function registrar_vitrine() {
	$descricao = 'Usado para listar as Vitrines';
	$singular = 'Vitrines';
	$plural = 'Vitrines';

	$labels = array(
		'name' => $plural,
		'singular_name' => $singular,
		'view_item' => 'Ver ' . $singular,
		'edit_item' => 'Editar ' . $singular,
		'new_item' => 'Novo ' . $singular,
		'add_new_item' => 'Adicionar nova ' . $singular
	);

	$supports = array(
		'title',
		'thumbnail'
	);

	$args = array(
		'labels' => $labels,
		'description' => $descricao,
		'public' => true,
        'rewrite' => true,
    'capability_type' => 'post',
         'capability_type' => 'post',
      
  'map_meta_cap' => true, 
		'menu_icon' => 'dashicons-images-alt2',
		'supports' => $supports,
        'rewrite' => array('slug' => 'vitrines')
	);


	register_post_type('vitrines', $args);	
}

add_action('init', 'registrar_vitrine');



// Banners Renda
function registrar_bannersRenda() {
	$descricao = 'Usado para listar a os Banners Renda';
	$singular = 'Banner Renda';
	$plural = 'Banners Renda';

	$labels = array(
		'name' => $plural,
		'singular_name' => $singular,
		'view_item' => 'Ver ' . $singular,
		'edit_item' => 'Editar ' . $singular,
		'new_item' => 'Novo ' . $singular,
		'add_new_item' => 'Adicionar nova ' . $singular
	);

	$supports = array(
		'title',
		'thumbnail'
	);

	$args = array(
		'labels' => $labels,
		'description' => $descricao,
		'public' => true,
        'exclude_from_search' => true,
        'rewrite' => true,
    'capability_type' => 'post',
         'capability_type' => 'post',
      
  'map_meta_cap' => true, 
		'menu_icon' => 'dashicons-images-alt2',
		'supports' => $supports,
        'rewrite' => array('slug' => 'banners')
	);


	register_post_type('banners', $args);	
}

add_action('init', 'registrar_bannersRenda');




// Banners Cambio
function registrar_bannersCambio() {
	$descricao = 'Usado para listar a os Banners Cambio';
	$singular = 'Banner Cambio';
	$plural = 'Banners Cambio';

	$labels = array(
		'name' => $plural,
		'singular_name' => $singular,
		'view_item' => 'Ver ' . $singular,
		'edit_item' => 'Editar ' . $singular,
		'new_item' => 'Novo ' . $singular,
		'add_new_item' => 'Adicionar nova ' . $singular
	);

	$supports = array(
		'title',
		'thumbnail'
	);

	$args = array(
		'labels' => $labels,
		'description' => $descricao,
		'public' => true,
         'exclude_from_search' => true,
        'rewrite' => true,
    'capability_type' => 'post',
         'capability_type' => 'post',
      
  'map_meta_cap' => true, 
		'menu_icon' => 'dashicons-images-alt2',
        'exclude_from_search' => true, 
		'supports' => $supports,
        'rewrite' => array('slug' => 'bannersCambio')
	);


	register_post_type('bannersCambio', $args);	
}

add_action('init', 'registrar_bannersCambio');


// Banners Criptomoeda
function registrar_bannersCriptomoeda() {
	$descricao = 'Usado para listar a os Banner Glossário Financeiro';
	$singular = 'Banner Glossário Financeiro';
	$plural = 'Banner Glossário Financeiro';

	$labels = array(
		'name' => $plural,
		'singular_name' => $singular,
		'view_item' => 'Ver ' . $singular,
		'edit_item' => 'Editar ' . $singular,
		'new_item' => 'Novo ' . $singular,
		'add_new_item' => 'Adicionar nova ' . $singular
	);

	$supports = array(
		'title',
		'thumbnail'
	);

	$args = array(
		'labels' => $labels,
		'description' => $descricao,
		'public' => true,
        'rewrite' => true,
    'capability_type' => 'post',
         'exclude_from_search' => true,
        'exclude_from_search' => true, 
         'capability_type' => 'post',
      
  'map_meta_cap' => true, 
		'menu_icon' => 'dashicons-images-alt2',
		'supports' => $supports,
        'rewrite' => array('slug' => 'bannersCambio')
	);


	register_post_type('bannersCriptomoeda', $args);	
}

add_action('init', 'registrar_bannersCriptomoeda');



// Banners Planos Investimentos
function registrar_bannersInvestimentos() {
	$descricao = 'Usado para listar a os Banners Planos Investimentos';
	$singular = 'Banners Planos Investimentos';
	$plural = 'Banners Planos Investimentos';

	$labels = array(
		'name' => $plural,
		'singular_name' => $singular,
		'view_item' => 'Ver ' . $singular,
		'edit_item' => 'Editar ' . $singular,
		'new_item' => 'Novo ' . $singular,
		'add_new_item' => 'Adicionar nova ' . $singular
	);

	$supports = array(
		'title',
		'thumbnail'
	);

	$args = array(
		'labels' => $labels,
		'description' => $descricao,
		'public' => true,
        'exclude_from_search' => true, 
        'rewrite' => true,
    'capability_type' => 'post',
         'capability_type' => 'post',
      
  'map_meta_cap' => true, 
		'menu_icon' => 'dashicons-images-alt2',
		'supports' => $supports,
        'rewrite' => array('slug' => 'bannersInvestimento')
	);


	register_post_type('bannersInvestimento', $args);	
}

add_action('init', 'registrar_bannersInvestimentos');




add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
show_admin_bar(false);


function title_excerpt($maxchars) {
    $title = get_the_title($post->ID);
    $title = substr($title,0,$maxchars);
    echo $title.'...';
}

function title_limite($maximo){
	$title = get_the_title();
	if( strlen($title)>$maximo){
		$continua='...';
	}
	$title=mb_substr($title,0,$maximo,'UTF-8');
	echo $title.$continua;
}

function wp_limit_post($max_char, $more_link_text = '[...]',$notagp = false, $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);
 
   if (strlen($_GET['p']) > 0) {
      if($notagp) {
      echo substr($content,0,$max_char);
      }
      else {
      echo '<p>';
      echo substr($content,0,$max_char);
      echo "</p>";
      }
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
        $content = substr($content, 0, $espacio);
        $content = $content;
        if($notagp) {
        echo substr($content,0,$max_char);
        echo $more_link_text;
        }
        else {
        echo '<p>';
        echo substr($content,0,$max_char);
        echo $more_link_text;
        echo "</p>";
        }
   }
   else {
      if($notagp) {
      echo substr($content,0,$max_char);
      }
      else {
      echo '<p>';
      echo substr($content,0,$max_char);
      echo "</p>";
      }
   }
}




//Logo admin

function custom_login_css() {
echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/build/css/geral.min.css"/>';
}
add_action('login_head', 'custom_login_css');


/*Função que altera a URL, trocando pelo endereço do seu site*/
function my_login_logo_url() {
return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
 
/*Função que adiciona o nome do seu site, no momento que o mouse passa por cima da logo*/
function my_login_logo_url_title() {
return 'Nome do seu site - Voltar para Home';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
function wpb_move_comment_field_to_bottom( $fields ) {
  $comment_field = $fields['comment'];
  unset( $fields['comment'] );
  $fields['comment'] = $comment_field;
  return $fields;
}
add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );


function mytheme_comment($comment, $args, $depth) {

   $GLOBALS['comment'] = $comment; ?>

<li <?php comment_class(); ?> id="li-comment-
    <?php comment_ID() ?>">
    <div id="comment-<?php comment_ID(); ?>" class="cf">
        <div class="comment_avatar grid-15 tablet-grid-20">
            <?php echo get_avatar($comment,$size='109' ); ?>
        </div>

        <div class="comment_infos grid-85 tablet-grid-80">
            <span class="comment_autor font-black">
                <?= comment_author(); ?>
            </span>

            <span class="comment_data">
                <?= get_comment_date(); ?>
                as
                <?= get_comment_time(); ?>
            </span>

            <span class="aprovacao ">
                <?php if ($comment->comment_approved == '0') : ?>
                <em>
                    <?php _e('Your comment is awaiting moderation.') ?></em>
                <?php endif; ?>
            </span>

            <span class="comment_text db font-regular">
                <?= comment_text(); ?>
            </span>

            <span class="resp db">
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
            </span>

        </div>

    </div>

    <?php }


// Minificar html
class FLHM_HTML_Compression
    {
    protected $flhm_compress_css = true;
    protected $flhm_compress_js = true;
    protected $flhm_info_comment = true;
    protected $flhm_remove_comments = true;
    protected $html;
    public function __construct($html)
    {
if (!empty($html))
{
$this->flhm_parseHTML($html);
}
}
public function __toString()
{
return $this->html;
}
protected function flhm_bottomComment($raw, $compressed)
{
$raw = strlen($raw);
$compressed = strlen($compressed);
$savings = ($raw-$compressed) / $raw * 100;
$savings = round($savings, 2);
return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
}
protected function flhm_minifyHTML($html)
{
$pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
$overriding = false;
$raw_tag = false;
$html = '';
foreach ($matches as $token)
{
$tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
$content = $token[0];
if (is_null($tag))
{
if ( !empty($token['script']) )
{
$strip = $this->flhm_compress_js;
}
else if ( !empty($token['style']) )
{
$strip = $this->flhm_compress_css;
}
else if ($content == '<!--wp-html-compression no compression-->')
{
$overriding = !$overriding; 
continue;
}
else if ($this->flhm_remove_comments)
{
if (!$overriding && $raw_tag != 'textarea')
{
$content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
}
}
}
else
{
if ($tag == 'pre' || $tag == 'textarea')
{
$raw_tag = $tag;
}
else if ($tag == '/pre' || $tag == '/textarea')
{
$raw_tag = false;
}
else
{
if ($raw_tag || $overriding)
{
$strip = false;
}
else
{
$strip = true; 
$content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
$content = str_replace(' />', '/>', $content);
}
}
} 
if ($strip)
{
$content = $this->flhm_removeWhiteSpace($content);
}
$html .= $content;
} 
return $html;
} 
public function flhm_parseHTML($html)
{
$this->html = $this->flhm_minifyHTML($html);
if ($this->flhm_info_comment)
{
$this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
}
}
protected function flhm_removeWhiteSpace($str)
{
$str = str_replace("\t", ' ', $str);
$str = str_replace("\n",  '', $str);
$str = str_replace("\r",  '', $str);
while (stristr($str, '  '))
{
$str = str_replace('  ', ' ', $str);
}   
return $str;
}
}
function flhm_wp_html_compression_finish($html)
{
return new FLHM_HTML_Compression($html);
}
function flhm_wp_html_compression_start()
{
ob_start('flhm_wp_html_compression_finish');
}
add_action('get_header', 'flhm_wp_html_compression_start');

 $favicon_url = get_template_directory_uri() . '/favicon.icon';

function add_admin_favicon() { echo '<link rel="shortcut icon" href="'.$favicon_url.'" />'; } add_action('login_head', 'add_admin_favicon'); add_action('admin_head', 'add_admin_favicon');
?>
