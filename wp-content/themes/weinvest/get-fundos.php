<?php 


/**
 * The template name: Página Temporaria
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */


        require 'spreadsheet/vendor/autoload.php';
        
        //Busca as informações da plainha de mercado
        $planilhaMercado ='./prevMercado.xlsx';
        $dataMercado = \PhpOffice\PhpSpreadsheet\IOFactory::load($planilhaMercado);
        $dataTable_mercado = $dataMercado->getActiveSheet()->toArray();
    
        //Busca as informações da plainha de XPCS
        $planilhaXPCS ='./prevXPCS.xlsx';
        $dataXPCS = \PhpOffice\PhpSpreadsheet\IOFactory::load($planilhaXPCS);
        $dataTable_XPCS = $dataXPCS->getActiveSheet()->toArray();
        
        //CNPJ de entrada do usuário
        $cnpj  = $_POST['cnpj'];
    
        //Encontro o cnpj na tabela mercado
        $mercado = array();
        $tamArray = count( $dataTable_mercado );
        for( $i = 0; $i < $tamArray; $i++ ){
            array_push(
                $mercado, 
                array( "label" => $dataTable_mercado[$i][0] , "value" => $dataTable_mercado[$i][50] )
                
            );
          
        }
     
            //var_dump($mercado);

        //$dados_json = json_encode($mercado);
        
        //echo $dados_json;   
    
    
?>
