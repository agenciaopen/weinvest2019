<?php

/**
 * The template name: Simple
 */


    require 'spreadsheet/vendor/autoload.php';

    function acrescimo($valor, $porcentagem, $potencia)
    {

        $fator = (1 + ($porcentagem / 100));
        $val_potencia = pow($fator, $potencia);
        $resultado = $valor * $val_potencia;

        $resultado = round($resultado, 2);

        return $resultado;
    }

    function getPremissa($campo)
    {
        $caminho_planilha = './planilha.xlsx';

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($caminho_planilha);

        $val_inv = $spreadsheet->getActiveSheet()->getCell($campo)->getFormattedValue();
        $val = str_replace("%", "", $val_inv);
        return $val;
    }

    function getValores($val, $prazo, $premissa)
    {
        $valor_sugerido = $val;
        $arr = array();

        for ($i = 1; $i <= $prazo; $i++) {
            array_push($arr, $valor_sugerido);
            $valor_sugerido = acrescimo($val, $premissa, $i + 1);
        }

        return $arr;
    }

    function concatValores($prazo, $lista1, $lista2, $lista3)
    {
        $valores = array();

        for ($i = 0; $i < $prazo; $i++) {
            array_push(
                $valores,
                array("inv" => $lista1[$i], "cdi" => $lista2[$i], "poup" => $lista3[$i])
            );
        }

        return $valores;
    }

    function getJson($val)
    {
        //apaga o arquivo existente para gerar um com novos dados
        array_map('unlink', glob("valores.json"));

        // Tranforma o array $dados_identificador em JSON
        $dados_json = json_encode($val);

        // Abre ou cria o arquivo contato.json
        // "a" indicar que o arquivo é aberto para ser escrito
        $fp = fopen("valores.json", "a+");

        // Escreve o conteúdo JSON no arquivo
        $escreve = fwrite($fp, $dados_json);

        // Fecha o arqui
        fclose($fp);
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        require 'spreadsheet/vendor/autoload.php';

        global $spreadsheet;

        $caminho_planilha = './planilha.xlsx';
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($caminho_planilha);

        $val_cdi = $spreadsheet->getActiveSheet()->getCell('I12')->getFormattedValue();
        $val_poupanca = $spreadsheet->getActiveSheet()->getCell('I11')->getFormattedValue();

        $tableReferencePremissa = [
            "conservador" => [
                6   => "G4",
                12  => "H4",
                24  => "I4",
                36  => "J4",
                60  => "K4",
                96  => "L4",
                120 => "M4",
                180 => "N4",
            ],
            "conservador-moderado" => [
                "6"   => "G5",
                "12"  => "H5",
                "24"  => "I5",
                "36"  => "J5",
                "60"  => "K5",
                "96"  => "L5",
                "120" => "M5",
                "180" => "N5",
            ],
            "moderado" => [
                "6"   => "G6",
                "12"  => "H6",
                "24"  => "I6",
                "36"  => "J6",
                "60"  => "K6",
                "96"  => "L6",
                "120" => "M6",
                "180" => "N6",
            ],
            "agressivo" => [
                "6"   => "G7",
                "12"  => "H7",
                "24"  => "I7",
                "36"  => "J7",
                "60"  => "K7",
                "96"  => "L7",
                "120" => "M7",
                "180" => "N7",
            ],
        ];

        $valor_de_entrada = 0;
        $meses = "";
        $perfil = "";


        if (!empty($_POST["valor"])) {
            $valor_de_entrada = (int)$_POST["valor"];
        }
        if (!empty($_POST["prazo"])) {
            $meses = (int)$_POST["prazo"];
        }
        if (!empty($_POST["perfil"])) {
            $perfil = (string)$_POST["perfil"];
        }

        $premissa = getPremissa($tableReferencePremissa[$perfil][$meses]);

        $inv_sugerido = getValores($valor_de_entrada, $meses, $premissa);

        $cdi = getValores($valor_de_entrada, $meses, $val_cdi);

        $poupanca = getValores($valor_de_entrada, $meses, $val_poupanca);

        $valores = concatValores( $meses, $inv_sugerido, $cdi, $poupanca);



        $dados_json = json_encode($valores);

        echo $dados_json;

    }
?>
