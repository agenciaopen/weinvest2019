<?php
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        require 'spreadsheet/vendor/autoload.php';
        
        //Busca as informações da plainha de mercado
        $planilhaMercado ='./prevMercado.xlsx';
        $dataMercado = \PhpOffice\PhpSpreadsheet\IOFactory::load($planilhaMercado);
        $dataTable_mercado = $dataMercado->getActiveSheet()->toArray();
    
        //Busca as informações da plainha de XPCS
        $planilhaXPCS ='./prevXPCS.xlsx';
        $dataXPCS = \PhpOffice\PhpSpreadsheet\IOFactory::load($planilhaXPCS);
        $dataTable_XPCS = $dataXPCS->getActiveSheet()->toArray();
        
        //CNPJ de entrada do usuário
        $cnpj  = $_POST['cnpj'];
    
        //Encontro o cnpj na tabela mercado
        $mercado = array();
        $tamArray = count( $dataTable_mercado );
        for( $i = 0; $i < $tamArray; $i++ ){
            array_push(
                $mercado, 
                array( "fundo" => $dataTable_mercado[$i][0], "cnpj" => $dataTable_mercado[$i][50], "cnpjSugerido" => $dataTable_mercado[$i][51], "12meses" => $dataTable_mercado[$i][28], "24meses" => $dataTable_mercado[$i][29], "36meses" => $dataTable_mercado[$i][30], "meses" => $dataTable_mercado[$i][26] )
            );
        }
    
        //Encontro o cnpj na tabela XPCS
        $xpcs = array();
        $tamArray2 = count( $dataTable_XPCS );
        for( $i = 0; $i < $tamArray2; $i++ ){
            array_push(
                $xpcs, 
                array( "fundo" => $dataTable_XPCS[$i][0], "cnpj" => $dataTable_XPCS[$i][13], "12meses" => $dataTable_XPCS[$i][34], "24meses" => $dataTable_XPCS[$i][35], "36meses" => $dataTable_XPCS[$i][36], "meses" => $dataTable_mercado[$i][32] )
            );
        }
    
        $idCpnjInserido = array_search($cnpj , array_column($mercado, 'cnpj'));
       if (false !== $idCpnjInserido )
            {
             $idCnpjComparado = array_search($mercado[$idCpnjInserido]['cnpjSugerido'], array_column($xpcs, 'cnpj'));
                        unset(array_column($mercado, 'cnpj')[$idCpnjInserido]);

        
        
        $fundo_selecionado = $mercado[$idCpnjInserido];
        $fundo_sugerido = $xpcs[$idCnpjComparado];
       
        $concat = $fundo_selecionado + $fundo_sugerido;

        $dados_json = json_encode(array($fundo_selecionado,$fundo_sugerido));
        
        echo $dados_json;   
}
    }
?>