<!doctype html>
<html>

<head>
    <title>
        <?php wp_title(''); ?>
    </title>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/favicon.ico" />
<meta name="google-site-verification" content="f-QVvu7ZC1sjPBbfNtO11Z3n4ydTuu-2oXJxsjkBp9c" />


    <?php if (is_front_page() && is_home()) { ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/build/css/home.min.css">

    <?php } else { ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/build/css/internas.min.css">
    <?php }  ?>
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <?php wp_head(); ?>
    
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TTF4D7R');</script>

    
</head>

<body <?php body_class(); ?>>
    
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTF4D7R"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <section class="loading text-center">
        <div class="box-loading">
            <div class="vertical">
                <img src="<?= get_template_directory_uri(); ?>/build/imgs/loading.gif" alt="" width="200">
            </div>
        </div>
    </section>
    <section class="busca">
        <a href="#" class="fecha-busca f-right">
            <i class="icon-cancel"></i>
        </a>
        <div class="box-search">
            <div class="">
                <?php get_search_form(); ?>
            </div>
        </div>
    </section>

    <header class="header cf">
        <div class="grid-container">
            <!-- Top header - busca - mobile -->
            <div class="content-header cf hide-on-desktop">
                <div class="mobile-grid-25 tablet-grid-35">
                    <button class="btn-menu">
                        <span class="bar"></span>
                        <span class="text">Menu</span>
                    </button>

                    <div class="overlay">
                        <nav class="cf">
                            <ul>
                                <li><a href="<?= get_site_url(); ?>/"><i class="icon-right-open-mini"></i>Home</a></li>
                                <li><a href="<?= get_site_url(); ?>/categoria/como-investir/"><i class="icon-right-open-mini"></i>Como Investir</a></li>
                                <li><a href="<?= get_site_url(); ?>/categoria/renda-fixa/"><i class="icon-right-open-mini"></i>Renda Fixa</a></li>
                                <li><a href="<?= get_site_url(); ?>/categoria/tesouro-direto/"><i class="icon-right-open-mini"></i>Tesouro Direto</a></li>
                                <li><a href="<?= get_site_url(); ?>/categoria/acoes/"><i class="icon-right-open-mini"></i>Ações</a></li>
                                <li><a href="<?= get_site_url(); ?>/categoria/fundos-de-investimentos/"><i class="icon-right-open-mini"></i>Fundos de Investimentos</a></li>
                                <li><a href="<?= get_site_url(); ?>/categoria/mercado-e-noticias/"><i class="icon-right-open-mini"></i>Mercado e Notícias</a></li>
                                <li><a href="<?= get_site_url(); ?>/categoria/planejamento-financeiro/"><i class="icon-right-open-mini"></i>Planejamento Financeiro</a></li>
                                <li><a href="<?= get_site_url(); ?>/quem-somos"><i class="icon-right-open-mini"></i>Quem somos</a> </li>
                                <li><a href="<?= get_site_url(); ?>/contato"><i class="icon-right-open-mini"></i> <span>Contato</span> </a>
                                </li>
                                <li><a href="<?= get_site_url(); ?>/glossario-weinvest/"><i class="icon-right-open-mini"></i> <span>Glossário </span> </a>

                                </li>

                            </ul>
                        </nav>
                        <div class="social-menu">

                            <h2>Redes Sociais</h2>
                            <ul>
                                <li><a href=""><i class="icon-facebook"></i></a></li>
                                <li><a href=""><i class="icon-instagram"></i></a></li>
                                <li><a href=""><i class="icon-youtube-play"></i></a></li>
                                <li><a href="https://twitter.com/WeInvestTweet"><i class="icon-twitter"></i></a></li>
                            </ul>
                        </div>

                        <!-- <div class="news-menu">
                            <div class="form cf">
                                <h2>Assine nossa newsletter</h2>
                                <label for="Nome">Nome:</label>
                                <input type="text" id="User" name="Nome" />
                                <label for="Nome">E-mail:</label>
                                <input type="email" id="E-mail" name="E-mail" />
                                <input type="submit" vale="Enviar" class="btn" />
                            </div>
                        </div><!-- end overlay -->
                    </div>
                </div>
                <div class="mobile-grid-50 tablet-grid-30">
                    <a href="<?= get_site_url(); ?>/">
                        <img src="<?php bloginfo('template_url') ?>/build/imgs/logo.svg" alt="" class="logo" />
                    </a>
                </div>
                <div class="mobile-grid-25 tablet-grid-35">
                    <a href="#" class="abrir-busca">
                        <i class="icon-lupa"></i>
                    </a>
                </div>
            </div>

            <!-- Top header - busca - desk -->
            <div class="content-header-desk hide-on-tablet hide-on-mobile">
                <div class="h-top cf">
                    <div class="grid-20">
                        <h1>
                            <a href="<?= get_site_url(); ?>/">
                                <img src="<?php bloginfo('template_url') ?>/build/imgs/logo-colorido.png" alt="" />
                            </a>
                        </h1>
                    </div>
                    <div class="grid-50 prefix-20">
                        <section class="box-banner cf">
                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                            'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();

                                        $imageBannerm = get_field('imagem-desk');
                                        $size = 'full';
                                        if (!empty($imageBannerm)) : ?>
                                    <a href="<?= get_field('link'); ?>">
                                        <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                    </a>
                                    <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>

                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                          'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();
                                        $imageBannerm = get_field('imagem_celular');
                                        $size = 'full';

                                        if (!empty($imageBannerm)) : ?>
                                    <a href="<?= get_field('link'); ?>">
                                        <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                    </a>
                                    <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>
                        </section>
                    </div>
                    <div class="grid-10">
                        <div class="search relative">
                            <a href="#" class="abrir-busca">
                                <i class="icon-lupa"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <nav class="nav-desk cf">

                    <ul class="nav-menu">
                        <li class="como-investir"><a href="<?= get_site_url(); ?>/categoria/como-investir/" class="uppercase">Como investir</a>
                            <?php
                                $childCats = get_categories(array(
                                    'orderby'  => 'name',
                                    'child_of' => 2
                                ));
                                if(!empty($childCats)) : ?>
                            <ul class="nav-submenu">
                                <?php foreach ($childCats as $childcat) { ?>
                                <li>
                                    <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                        <?= $childcat->cat_name; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php endif; ?>
                        </li>



                        <li class="renda-fixa"> <a href="<?= get_site_url(); ?>/categoria/renda-fixa" class="uppercase"> Renda fixa</a>
                            <?php
                                $childCats = get_categories(array(
                                    'orderby'  => 'name',
                                    'child_of' => 1
                                ));
                                if(!empty($childCats)) : ?>
                            <ul class="nav-submenu">
                                <?php foreach ($childCats as $childcat) { ?>
                                <li>
                                    <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                        <?= $childcat->cat_name; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php endif; ?>
                        </li>

                        <li class="tesouro-direto"><a href="<?= get_site_url(); ?>/categoria/tesouro-direto" class="uppercase">Tesouro direto</a>
                            <?php
                                $childCats = get_categories(array(
                                    'orderby'  => 'name',
                                    'child_of' => 4
                                ));
                                if(!empty($childCats)) : ?>
                            <ul class="nav-submenu">
                                <?php foreach ($childCats as $childcat) { ?>
                                <li>
                                    <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                        <?= $childcat->cat_name; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php endif; ?>
                        </li>

                        <li class="acoes"><a href="<?= get_site_url(); ?>/categoria/acoes" class="uppercase">Ações</a>
                            <?php
                                $childCats = get_categories(array(
                                
                                    'child_of' => 32,
                                    'orderby' => 'id'
                                ));
                                if(!empty($childCats)) : ?>
                            <ul class="nav-submenu">
                                <?php foreach ($childCats as $childcat) { ?>
                                <li>
                                    <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                        <?= $childcat->cat_name; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php endif; ?>
                        </li>

                        <li class="fundos-de-investimentos"><a href="<?= get_site_url(); ?>/categoria/fundos-de-investimentos/" class="uppercase">Fundos de investimentos</a>
                            <?php
                                $childCats = get_categories(array(
                                    'orderby'  => 'name',
                                    'child_of' => 5
                                ));
                                if(!empty($childCats)) : ?>
                            <ul class="nav-submenu">
                                <?php foreach ($childCats as $childcat) { ?>
                                <li>
                                    <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                        <?= $childcat->cat_name; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php endif; ?>
                        </li>

                        <li class=""><a href="<?= get_site_url(); ?>/categoria/mercado-e-noticias" class="uppercase"> Mercado e notícias</a>
                            <?php
                                $childCats = get_categories(array(
                                    'orderby'  => 'name',
                                    'child_of' => 6
                                ));
                                if(!empty($childCats)) : ?>
                            <ul class="nav-submenu">
                                <?php foreach ($childCats as $childcat) { ?>
                                <li>
                                    <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                        <?= $childcat->cat_name; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php endif; ?>
                        </li>

                        <li class=""><a href="<?= get_site_url(); ?>/categoria/planejamento-financeiro" class="uppercase">Planejamento Financeiro</a>
                            <?php
                                $childCats = get_categories(array(
                                    'orderby'  => 'id',
                                    'child_of' => 7
                                ));
                                if(!empty($childCats)) : ?>
                            <ul class="nav-submenu">
                                <?php foreach ($childCats as $childcat) { ?>
                                <li>
                                    <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                        <?= $childcat->cat_name; ?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                            <?php endif; ?>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </header>
