<?php

get_header();
?>
<div class="pg-home cf">
    <div class="grid-container grid-parent">
        <div class="tablet-grid-60 tablet-prefix-20 tablet-suffix-20 grid-70 prefix-15 suffix-30 hide-on-desktop">
              <section class="box-banner cf">
                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                            'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();

                                        $imageBannerm = get_field('imagem-desk');
                                        $size = 'full';
                                        if (!empty($imageBannerm)) : ?>
                                            <a href="<?= get_field('link'); ?>">
                                                <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                            </a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>

                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                          'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();
                                        $imageBannerm = get_field('imagem_celular');
                                        $size = 'full';

                                        if (!empty($imageBannerm)) : ?>
                                            <a href="<?= get_field('link'); ?>">
                                                <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                            </a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>
                        </section>
        </div>

        <!-- Vitrine  -->
        <div class="mobile-grid-100 tablet-grid-60 grid-parent grid-55">
            <section class="post-dest cf">

                <?php // WP_Query arguments
                    $args = array(
                        'posts_per_page'         => '1',
                        'meta_query'	=> array(
                            'relation'		=> 'AND',
                            array(
                                'key'	 	=> 'vitrine',
                                'value'	  	=> true,
                                'compare' 	=> '=',
                            ),
                            array(
                                'key'	 	=> 'post_principal',
                                'value'	  	=> true,
                                'compare' 	=> '=',
                            )
                        ),
                    );

                    // The Query
                    $vitrine = new WP_Query( $args ); 
                    if ( $vitrine->have_posts() ) : 
                        while ( $vitrine->have_posts() ) : $vitrine->the_post();

                            $category = get_the_category();

                            $catParent = $category[0]->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                $catId  = $catPai->cat_ID;
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catId   = $category[0]->cat_ID;
                                $catName = $category[0]->name;
                                $catSlug = $category[0]->slug;
                            }
                        ?>
                    
                            <a href="<?php the_permalink()?>" class="setCookie" data-cat="<?= $catId; ?>">
                                <figure class="p-figure" itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">

                                    <figcaption itemprop="caption description" class="caption">
                                        <span class="post-cat <?= $catSlug;?>">
                                            <?= $catName; ?>
                                        </span>
                                        <span class="post-name <?= $catSlug; ?>">
                                            <?= title_limite(70); ?></span>
                                        <span class="post-date">
                                            <?= get_the_date(); ?></span>
                                    </figcaption>
                                </figure>
                            </a>
                    <?php endwhile; 
                        wp_reset_query(); 
                    endif; ?>

            </section>
        </div>

        <div class="tablet-grid-40 grid-parent grid-45">
            <section class="post-sec cf">
                <div class="slider-post-sec">
                    <?php // WP_Query arguments
                            $args2 = array(
                                'posts_per_page'         => '3',
                                'meta_query'	=> array(
                                    'relation'		=> 'AND',
                                    array(
                                        'key'	 	=> 'vitrine',
                                        'compare' 	=> '=',
                                        'value'	  	=> true
                                    ),
                                    array(
                                        'key'	 	=> 'post_principal',
                                        'compare' 	=> '!=',
                                        'value'	  	=> true
                                    )
                                ),
                            );

                            // The Query
                            $vitrine2 = new WP_Query( $args2 ); 

                            if ( $vitrine2->have_posts() ) : 
                                while ( $vitrine2->have_posts() ) : $vitrine2->the_post(); 

                                $category = get_the_category();
                                $catParent = $category[0]->parent;
                                $catName = "";
                                $catSlug = "";

                                if( $catParent > 0){
                                    $catPai = get_category( $catParent );
                                    $catId  = $catPai->cat_ID;
                                    $catName = $catPai->name;
                                    $catSlug = $catPai->slug;
                                }else{
                                    $catId   = $category[0]->cat_ID;
                                    $catName = $category[0]->name;
                                    $catSlug = $category[0]->slug;
                                }

                                if( $cont == 0): ?>
                                    <div>
                                        <div class="grid-100 grid-parent">
                                            <a href="<?php the_permalink()?>" class="setCookie" data-cat="<?= $catId; ?>">
                                                <figure class="s-figure maior" itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                    <figcaption itemprop="caption description" class="caption">
                                                        <span class="post-cat <?= $catSlug; ?>">
                                                            <?= $catName; ?>
                                                        </span>

                                                        <span class="post-name <?= $catSlug; ?>">
                                                            <?= title_limite(70); ?></span>
                                                        <span class="post-date">
                                                            <?= get_the_date(); ?></span>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div>
                                        <div class="grid-50 grid-parent">
                                            <a href="<?php the_permalink()?>" class="setCookie" data-cat="<?= $catId; ?>">
                                                <figure class="s-figure maior" itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                    <figcaption itemprop="caption description" class="caption">
                                                        <span class="post-cat <?= $catSlug; ?>">
                                                            <?= $catName; ?>
                                                        </span>

                                                        <span class="post-name <?= $catSlug; ?>">
                                                            <?= title_limite(70); ?></span>
                                                        <span class="post-date">
                                                            <?= get_the_date(); ?></span>
                                                    </figcaption>
                                                </figure>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; 
                                $cont++; 
                            endwhile; 
                            wp_reset_query(); 
                        endif; 
                    ?>
                </div>
            </section>
        </div>
        <!-- Vitrine  -->

        <!-- Main  -->
        <div class="tablet-grid-70 grid-parent grid-70 suffix-5">

            <!-- Select Categorias -->  
            <section class="cats cat-home cf">
                <div class="grid-container">
                    <div class="s-cat cf">
                        <div class="mobile-grid-50 tablet-grid-35 grid-30 grid-parent">
                            <h2>Categorias:</h2>
                        </div>
                        <div class="mobile-grid-50 grid-parent tablet-grid-65 grid-70">

                            <div class="dropdown toggle align-center">
                                <input id="t1" type="checkbox">
                                <label for="t1" id="botao">Selecione:</label>
                                <ul id="menu-cat">
                                     <li class=""><a class="mercado"> Mercado e notícias</a></li>
                                    <li class=""><a class="comoinvestir">Como investir</a></li>
                                    
                                    <li class=""><a class="renda">Renda fixa</a></li>
                                    <li class=""><a class="tesouro">Tesouro direto</a></li>
                                    <li class=""><a class="variavel">Ações</a></li>
                                    <li class=""><a class="fundo">Fundos de investimentos</a></li>
                                    <li class=""><a class="previdencia">Planejamento Financeiro </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="" id="conteudo-menu-cat">
                   

                    <div class="list-post-cat " id="mercado">

                        <?php 
                            $category = get_category(6);

                            $catParent = $category->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            }

                            $arMercado = array(
                                'cat' => 6,
                                'posts_per_page' => 4
                            );
                            $the_query_mercado = new WP_Query($arMercado);
                            if ( $the_query_mercado->have_posts() ) :
                                while ( $the_query_mercado->have_posts() ) : $the_query_mercado->the_post(); 
                                ?>
                                <div>
                                    <div class="box-posts setCookie" data-cat="6">
                                        <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName;?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                            <?= title_limite(60); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>

                    </div>
                     <div class="list-post-cat bx-none" id="comoinvestir">
                        <?php 
                            $category = get_category(2);

                            $catParent = $category->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            }

                            $args = array(
                                'cat' =>2,
                                'posts_per_page' => 4
                            );
                            $the_query = new WP_Query($args);
                        if ( $the_query->have_posts() ) : 
                            while ( $the_query->have_posts() ) : $the_query->the_post(); 
                                ?>

                                <div>
                                    <div class="box-posts setCookie" data-cat="2">
                                        <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                            <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName;?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                                <?= title_limite(60); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="list-post-cat bx-none" id="renda">
                        <?php 
                            $category = get_category(1);

                            $catParent = $category->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            }

                            $arMercado = array(
                                'cat' => 1,
                                'posts_per_page' => 4
                            );
                            $the_query_mercado = new WP_Query($arMercado);
                            if ( $the_query_mercado->have_posts() ) :
                                while ( $the_query_mercado->have_posts() ) : $the_query_mercado->the_post(); 
                                ?>
                                <div>
                                    <div class="box-posts setCookie" data-cat="1">
                                        <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName;?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                            <?= title_limite(60); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="list-post-cat bx-none" id="tesouro">
                        <?php 
                            $category = get_category(4);

                            $catParent = $category->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            }

                            $arMercado = array(
                                'cat' => 4,
                                'posts_per_page' => 4
                            );
                            $the_query_mercado = new WP_Query($arMercado);
                            if ( $the_query_mercado->have_posts() ) :
                                while ( $the_query_mercado->have_posts() ) : $the_query_mercado->the_post(); 
                                ?>
                                <div>
                                    <div class="box-posts setCookie" data-cat="4">
                                        <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName;?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                            <?= title_limite(60); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    
                    <div class="list-post-cat bx-none" id="variavel">
                        <?php 
                            $category = get_category(32);

                            $catParent = $category->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            }

                            $arMercado = array(
                                'cat' => 32,
                                'posts_per_page' => 4
                            );
                            $the_query_mercado = new WP_Query($arMercado);
                            if ( $the_query_mercado->have_posts() ) :
                                while ( $the_query_mercado->have_posts() ) : $the_query_mercado->the_post(); 
                                ?>
                                <div>
                                    <div class="box-posts setCookie" data-cat="32">
                                        <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName;?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                            <?= title_limite(60); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="list-post-cat bx-none" id="fundo">
                        <?php 
                            $category = get_category(5);

                            $catParent = $category->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            }

                            $arMercado = array(
                                'cat' => 5,
                                'posts_per_page' => 4
                            );
                            $the_query_mercado = new WP_Query($arMercado);
                            if ( $the_query_mercado->have_posts() ) :
                                while ( $the_query_mercado->have_posts() ) : $the_query_mercado->the_post(); 
                                ?>
                                <div>
                                    <div class="box-posts setCookie" data-cat="2">
                                        <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName;?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                            <?= title_limite(60); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    
                    <div class="list-post-cat bx-none" id="previdencia">
                     <?php 
                            $category = get_category(7);

                            $catParent = $category->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            }

                            $arMercado = array(
                                'cat' => 7,
                                'posts_per_page' => 4
                            );
                            $the_query_mercado = new WP_Query($arMercado);
                            if ( $the_query_mercado->have_posts() ) :
                                while ( $the_query_mercado->have_posts() ) : $the_query_mercado->the_post(); 
                                ?>
                                <div>
                                    <div class="box-posts setCookie" data-cat="7">
                                        <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                                <figcaption itemprop="caption description" class="caption">
                                                    <span class="post-cat <?= $catSlug; ?>">
                                                        <?= $catName;?>
                                                    </span>
                                                </figcaption>
                                            </figure>
                                            <h2 class="<?= $catSlug; ?>">
                                            <?= title_limite(60); ?>
                                            </h2>
                                            <span class="data">
                                                <?= get_the_date(); ?></span>
                                            <p>
                                                <?php wp_limit_post(150, '...', true); ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                </div>
            </section>
            
            <!-- Como Investir  -->
            <section class="cats cf">
                
                <?php 
                    $posts_not_in = array();
                    $my_query = new WP_Query(array(
                        'posts_per_page' => '2',
                        'cat' => 2,
                    )); 
                     if ( $my_query->have_posts() ) : ?>
                        
                            <section class="box-banner cf">
                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 1,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>

                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 1,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>
                            </section>

                        <div class="name-cat">
                            <div class="grid-container">
                                <a href="<?php bloginfo('siteurl') ?>/categoria/como-investir/" class="post-cat como-investir">
                                    Como Investir</a>
                                <hr class="como-investir" />
                            </div>
                        </div>
                        
                        <?php while ( $my_query->have_posts() ) : $my_query->the_post();
                            $category = get_the_category();
                            $parent = get_category($category[0]->category_parent);
                            array_push( $posts_not_in, get_the_ID() );
                        ?>
                        <div class="tablet-grid-50 grid-50 setCookie" data-cat="2">
                            <div class="box-list-cats box-posts">
                                <a href="<?php the_permalink(); ?>" title="">
                                    <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                        <figcaption itemprop="caption description" class="caption">
                                            <span class="post-cat como-investir">
                                            Como Investir
                                            </span>
                                        </figcaption>
                                    </figure>
                                    <h2 class="como-investir">
                                        <?= title_limite(70); ?> 
                                    </h2>
                                    <span class="data">
                                        <?=get_the_date(); ?></span>
                                    <p>
                                        <?php wp_limit_post(150,'...',true); ?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </section>

            <!-- Mercado e notícias  -->
            <section class="cats cf">
                <?php $queryMercado = new WP_Query(array(
                        'post__not_in' => $posts_not_in ,
                        'posts_per_page' => 2,
                        'cat' => 6,
                    ));  
                    if ( $queryMercado->have_posts() ) : ?>
                        
                        <section class="box-banner cf">
                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 3,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>

                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 3,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>
                            </section>

                        <div class="name-cat">
                            <div class="grid-container">
                                <a href="<?php bloginfo('siteurl') ?>/categoria/mercado-e-noticias" class="post-cat mercado-e-noticias">
                                    Mercado e Notícias</a>
                                <hr class="mercado-e-noticias" />
                            </div>
                        </div>
                        <?php while ( $queryMercado->have_posts() ) : $queryMercado->the_post(); 
                            $category = get_the_category();
                            $parent = get_category($category[0]->category_parent);
                            array_push( $posts_not_in, get_the_ID() );
                        ?>
                            <div class="tablet-grid-50 grid-50">
                                <div class="box-list-cats box-posts">
                                    <a href="<?php the_permalink(); ?>" title="">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                            <figcaption itemprop="caption description" class="caption">
                                                <span class="post-cat mercado-e-noticias">
                                                Mercado e notícias
                                                </span>
                                            </figcaption>
                                        </figure>
                                        <h2 class="mercado-e-noticias">
                                            <?= title_limite(70); ?>
                                        </h2>
                                        <span class="data">
                                            <?=get_the_date(); ?></span>
                                        <p>
                                            <?php wp_limit_post(150,'...',true); ?>
                                        </p>
                                    </a>
                                </div>
                            </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </section>
            
            <!-- Renda Fixa  -->
            <section class="cats ">
                <?php $queryRenda = new WP_Query(array(
                        'post__not_in' => $posts_not_in ,
                        'posts_per_page' => 2,
                        'cat' => 1,
                    )); 
                    if ( $queryRenda->have_posts() ) : ?>

                    <section class="box-banner cf">
                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 4,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>

                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 4,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>
                            </section>

                    <div class="name-cat">
                        <div class="grid-container">
                            <a href="<?php bloginfo('siteurl') ?>/categoria/renda-fixa" class="post-cat renda-fixa">
                                Renda Fixa </a>
                            <hr class="renda-fixa" />
                        </div>
                    </div>

                    <?php while ( $queryRenda->have_posts() ) : $queryRenda->the_post(); 
                        $category = get_the_category();
                        $parent = get_category($category[0]->category_parent);
                        array_push( $posts_not_in, get_the_ID() );
                    ?>
                        <div class="tablet-grid-50 grid-50 setCookie" data-cat="1">
                            <div class="box-list-cats box-posts">
                                <a href="<?php the_permalink(); ?>" title="">
                                    <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                        <figcaption itemprop="caption description" class="caption">
                                            <span class="post-cat renda-fixa">
                                            Renda Fixa
                                            </span>
                                        </figcaption>
                                    </figure>
                                    <h2 class="renda-fixa">
                                        <?= title_limite(70); ?>
                                    </h2>
                                    <span class="data">
                                        <?= get_the_date(); ?></span>
                                    <p>
                                        <?php wp_limit_post(150,'...',true); ?>
                                    </p>
                                </a>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>

            </section>
            
            <!-- Tesouro Direto -->
            <section class="cats cf">
                <?php $queryTesouro = new WP_Query(array(
                        'post__not_in' => $posts_not_in ,
                        'posts_per_page' => 2,
                        'cat' => 4,
                    )); 
                    if ( $queryTesouro->have_posts() ) : ?>

                       <section class="box-banner cf">
                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 5,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>

                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 5,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>
                            </section>

                        <div class="name-cat">
                            <div class="grid-container">
                                <a href="<?php bloginfo('siteurl') ?>/categoria/tesouro-direto" class="post-cat tesouro-direto">
                                    Tesouro Direto</a>
                                <hr class="tesouro-direto" />
                            </div>
                        </div>

                        <?php while ( $queryTesouro->have_posts() ) : $queryTesouro->the_post(); 
                            $category = get_the_category();
                            $parent = get_category($category[0]->category_parent);
                            array_push( $posts_not_in, get_the_ID() );
                        ?>
                
                            <div class="tablet-grid-50 grid-50 setCookie" data-cat="4">
                                <div class="box-list-cats box-posts">
                                <a href="<?php the_permalink(); ?>" title="">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                            <figcaption itemprop="caption description" class="caption">
                                                <span class="post-cat tesouro-direto">
                                                Tesouro Direto
                                                </span>
                                            </figcaption>
                                        </figure>
                                        <h2 class="tesouro-direto">
                                            <?= title_limite(70); ?>
                                        </h2>
                                        <span class="data">
                                            <?= get_the_date(); ?></span>
                                        <p>
                                            <?php wp_limit_post(150,'...',true); ?>
                                        </p>
                                    </a>
                                </div>
                            </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </section>

            <!-- Ações -->
            <section class="cats cf ">
                <?php $queryAcoes = new WP_Query(array(
                        'post__not_in' => $posts_not_in ,
                        'posts_per_page' => 2,
                        'cat' => 32,
                    ));
                    if ( $queryAcoes->have_posts() ) : ?>

                        <section class="box-banner cf">
                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 6,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>

                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 6,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>
                            </section>

                        <div class="name-cat">
                            <div class="grid-container">
                                <a href="<?php bloginfo('siteurl') ?>/categoria/renda-variavel" class="post-cat acoes">
                                    Ações</a>
                                <hr class="acoes" />
                            </div>
                        </div>
                        <?php while ( $queryAcoes->have_posts() ) : $queryAcoes->the_post(); 
                            $category = get_the_category();
                            $parent = get_category($category[0]->category_parent);
                            array_push( $posts_not_in, get_the_ID() );
                        ?>

                        <div class="tablet-grid-50 grid-50 setCookie" data-cat="32">
                            <div class="box-list-cats box-posts">
                                <a href="<?php the_permalink(); ?>" title="">
                                    <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">

                                        <figcaption itemprop="caption description" class="caption">
                                            <span class="post-cat acoes">
                                            Ações
                                            </span>
                                        </figcaption>
                                    </figure>
                                    <h2 class="acoes">
                                        <?= title_limite(70); ?>
                                    </h2>
                                    <span class="data">
                                        <?= get_the_date(); ?></span>
                                    <p>
                                        <?php wp_limit_post(150,'...',true);?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </section>
            
            <!-- Banner -->
            <section class="branch branch-cambio hide-on-desktop hide-on-desktop hide-on-tablet ">

                <div class="grid-container grid-parent">
                    <?php
                    $bannercr = new WP_Query( 
                        array(

                            'post_type'      => 'bannersCriptomoeda'
                        )
                    );

                    while ( $bannercr->have_posts() ) : $bannercr->the_post(); 

                        $imageBannercr = get_field('imagem');
                        $size = 'full';
                        if( !empty($imageBannercr) ): 

                        ?>
                    <a href="">
                        <img src="<?php echo $imageBannercr['url']; ?>" alt="" />
                    </a>

                    <?php endif; ?>
                    <?php endwhile; ?>

                </div>

            </section>

            <!-- Fundos de Investimentos -->
            <section class="cats cf">
                <?php $queryFundos = new WP_Query(array(
                        'post__not_in' => $posts_not_in ,
                        'posts_per_page' => 2,
                        'cat' => 5,
                    )); 
                    if ( $queryFundos->have_posts() ) : ?>
                        
                        <section class="box-banner cf">
                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 7,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>

                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 7,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>
                            </section>

                        <div class="name-cat">
                            <div class="grid-container">
                                <a href="<?php bloginfo('siteurl') ?>/categoria/fundos-investimentos" class="post-cat fundos-de-investimentos">
                                    Fundos de Investimentos</a>
                                <hr class="fundos-de-investimentos" />
                            </div>
                        </div>

                    <?php while ( $queryFundos->have_posts() ) : $queryFundos->the_post(); 
                        $category = get_the_category();
                        $parent = get_category($category[0]->category_parent);
                        array_push( $posts_not_in, get_the_ID() );
                    ?>
                
                        <div class="tablet-grid-50 grid-50 setCookie" data-cat="5">
                            <div class="box-list-cats box-posts">
                                <a href="<?php the_permalink(); ?>" title="">
                                    <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">

                                        <figcaption itemprop="caption description" class="caption">
                                            <span class="post-cat fundos-de-investimentos">
                                            Fundos de Investimentos
                                            </span>
                                        </figcaption>
                                    </figure>
                                    <h2 class="fundos-de-investimentos">
                                        <?= title_limite(70); ?>
                                    </h2>
                                    <span class="data">
                                        <?= get_the_date(); ?></span>
                                    <p>
                                        <?php wp_limit_post(150,'...',true);?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </section>

            <!-- Planejamento Financeiro -->
            <section class="cats cf ">
                <?php $queryPlanejamento = new WP_Query(array(
                        'post__not_in' => $posts_not_in ,
                        'posts_per_page' => 2,
                        'cat' => 7,
                    ));
                    if ( $queryPlanejamento->have_posts() ) : ?>

                       <section class="box-banner cf">
                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 8,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>

                                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                    <section class="banner cf">
                                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 8,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                                                <a href="<?= get_field('link'); ?>">
                                                    <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                                </a>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    </section>
                                </div>
                            </section>

                        <div class="name-cat">
                            <div class="grid-container">
                                <a href="<?php bloginfo('siteurl') ?>/categoria/previdencia-e-protecao" class="post-cat planejamento-financeiro">
                                    Planejamento Financeiro</a>
                                <hr class="planejamento-financeiro" />
                            </div>
                        </div>

                        <?php while ( $queryPlanejamento->have_posts() ) : $queryPlanejamento->the_post(); 
                            $category = get_the_category();
                            $parent = get_category($category[0]->category_parent);
                            array_push( $posts_not_in, get_the_ID() );
                        ?>
                    
                        <div class="tablet-grid-50 grid-50 setCookie" data-cat="7">
                            <div class="box-list-cats box-posts">
                                <a href="<?php the_permalink(); ?>" title="">
                                    <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">

                                        <figcaption itemprop="caption description" class="caption">
                                            <span class="post-cat planejamento-financeiro">
                                            Planejamento Financeiro
                                            </span>
                                        </figcaption>
                                    </figure>
                                    <h2 class="planejamento-financeiro">
                                        <?= title_limite(70); ?>
                                    </h2>
                                    <span class="data">
                                        <?= get_the_date(); ?></span>
                                    <p>
                                        <?php wp_limit_post(150,'...',true);?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </section>

        </div>
        <!-- Main  -->

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>
</div>
<?php
get_footer();
