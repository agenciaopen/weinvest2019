<?php 


/**
 * The template name: Página Contato
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';  ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna contato cf">
    <div class="grid-container">
        <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
            <section class="conteudo">
                <h1>  <?php the_title(); ?></h1>
                <?php the_content(); ?>
          
                <div class="list cf">
                    <ul>
                        <li class="cf">
                            <div class="mobile-grid-25 tablet-grid-25 grid-15">
                                <i class="icon-acessores"></i>
                            </div>
                            <div class="mobile-grid-75 tablet-grid-75 grid-85">
                                <p><strong><?= get_field('item_1') ?></strong></p>
                            </div>
                        </li>
                        <li class="cf">
                            <div class="mobile-grid-25 tablet-grid-25 grid-15">
                                <i class="icon-carteira"></i>
                            </div>
                            <div class="mobile-grid-75 tablet-grid-75 grid-85">
                                <p><strong><?= get_field('item_2') ?></strong></p>
                            </div>
                        </li>
                        <li class="cf">
                            <div class="mobile-grid-25  tablet-grid-25 grid-15">
                                <i class="icon-atendimento"></i>
                            </div>
                            <div class="mobile-grid-75  tablet-grid-75 grid-85">
                                <p><strong><?= get_field('item_3') ?></strong></p>
                            </div>
                        </li>
                        <li class="cf">
                            <div class="mobile-grid-25  tablet-grid-25 grid-15">
                                <i class="icon-cobranca"></i>
                            </div>
                            <div class="mobile-grid-75  tablet-grid-75 grid-85">
                                <p><strong><?= get_field('item_4') ?></strong></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <section class="form form-blue cf">
                <div class="grid-container">
                    <i class="icon-plano"></i>
                    <h2>Monte um plano para seu dinheiro com um assessor de investimentos</h2>
                    <?php echo do_shortcode( '[contact-form-7 id="1216" title="Página de Contato"]' ); ?>


                </div>
            </section>

        </div>
   
    <!-- Sidebar  -->
    <?php get_sidebar(); ?>
    <!-- /Sidebar  -->
 </div>

</div>

<?php endwhile; endif; ?>
<?php
get_footer();
?>
