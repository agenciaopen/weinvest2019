<?php 


/**
* The template name: Página GLossario
* @package   WordPress
* @subpackage   agenciaopen
* @since    2017
* @author:         Arlen Resende
* Projeto:        We Invest
* Data de Criação: 04/01/2018
* Version:   1.0
*/

include_once 'header.php';  ?>

<?php 

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna contato quem-somos cf">
    <div class="grid-container">
        <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
            <section class="conteudo">

                <h1>
                    <?php the_title(); ?>
                </h1>
                <?php the_content(); ?>
            </section>
            <section class="glossario ">

                <div id="accordion" class="accordion-container">

                    <?php 
                        $arr = range('A', 'Z');
                        $cont = 0;

                        $string = get_field('glossario');
                        //var_dump($string);
                        
                        foreach ($string as $key => $row) {
                            $the_title[$key] = $row['titulo_glossario'];
                            $the_desc[$key] = $row['conteudo_glossario'];

                        }
                       
                        array_multisort($the_title, SORT_ASC, $string);
                      
                        $variavelC = ''; ?>
                      

                            <article class="content-entry">
                                  <?php foreach(  $string as $row ) { ?>
                            
                                    <div class="box-glossario">
                                          <?php if ( $variavelC != $row['titulo_glossario'][0]) { ?>
                                        <h2 class="article-letter">

                                           <?php
                                                        echo $row['titulo_glossario'][0];
                                                        $variavelC = $row['titulo_glossario'][0];
                                                    
                                            ?>

                                        </h2>
                                        <?php } ?>
                                        <h3 class="article-title content-entry acc"><i></i>
                                            <?php echo $row['titulo_glossario']; ?>

                                        </h3>

                                        <div class="accordion-content">
                                            <?php echo $row['conteudo_glossario']; ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                            </article>
                   



                    <?php  $cont++;   ?>



                </div>
                <!--/#accordion-->


            </section>
            <section class="form form-blue cf">
                <div class="grid-container">
                    <i class="icon-plano"></i>
                    <h2>Gostaria de conversar sobre seus investimentos com um especialista?</h2>
                    <label for="Nome">Nome:</label>
                    <input type="text" id="User" name="Nome" />
                    <label for="Nome">E-mail:</label>
                    <input type="email" id="E-mail" name="E-mail" />
                    <label for="Telefone">Telefone:</label>
                    <input type="text" id="User" class='celular' name="Telefone" />
                    <label for="Valor">Qual o valor estimado de suas aplicações Financeiras? (Dinheiro, Moedas, Poupança, Aplicações em Bancos e Corretoras)*:</label>
                    <input type="number" id="Valor" name="Valor" />
                    <input type="submit" vale="Enviar" class="btn" />


                </div>
            </section>

        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>

</div>

<?php endwhile; endif;  wp_reset_query(); ?>
<?php
get_footer();
?>