<?php 


/**
 * The template name: Página Interna
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';  ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna single cf">
    <div class="grid-container">
        <div class="tablet-grid-70 grid-70 suffix-5">
            <div class="tablet-grid-80 tablet-prefix-10 tablet-suffix-10 hide-on-desktop">
                <section class="banner cf">
                    <div class="mobile-grid-15  grid-parent tablet-grid-10">
                        <i class="icon-celular"></i>
                    </div>
                    <div class="mobile-grid-45 grid-parent tablet-grid-55">
                        <p>Quer descobrir como
                            aumentar sua renda
                            em 1 mes?</p>
                    </div>
                    <div class="mobile-grid-40 grid-parent tablet-grid-35">
                        <a href="" class="btn btn-banner">Saiba mais</a>
                    </div>
                </section>
            </div>
            <section class="cats cf ">
                <div class="name-cat">
                    <div class="grid-container">
                        <a href="" class="post-cat">Como Investir</a>
                        <hr />
                    </div>
                </div>
                <div class="box-list-cats box-posts">

                    <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                    <span class="data">20 janeiro de 2019</span>
                    <img src="<?php bloginfo('template_url')?>/build/imgs/post-principal.jpg" height="" width="" itemprop="thumbnail" alt="Image description" />
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>


                </div>
            </section>
            <section class="user-post cf">
                <div class="grid-container">
                    <hr />
                    <div class="mobile-grid-35 tablet-grid-25 grid-25">
                        <img src="<?php bloginfo('template_url')?>/build/imgs/user.jpg" />
                    </div>
                    <div class="mobile-grid-65 tablet-grid-75 grid-75">
                        <h3>Elianela Horta Suares</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                    </div>
                    <hr />
                </div>
            </section>
            <section class="shared cf">
                <div class="grid-container">
                    <div class="social">
                        <div class="grid-container">
                            <h2>Compartilhe esse post</h2>
                            <ul>
                                <li>
                                    <i class="icon-facebook"></i>
                                </li>
                                <li>
                                    <i class="icon-instagram"></i>
                                </li>
                                <li>
                                    <i class="icon-youtube-play"></i>
                                </li>
                                <li>
                                    <i class="icon-twitter"></i>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>

            </section>
            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-90 prefix-10 suffix-10 ">
                <section class="banner cf">
                    <div class="mobile-grid-15  grid-parent tablet-grid-10 grid-10">
                        <i class="icon-celular"></i>
                    </div>
                    <div class="mobile-grid-45 grid-parent tablet-grid-55 grid-45 suffix-15">
                        <p>Quer descobrir como
                            aumentar sua renda
                            em 1 mes?</p>
                    </div>
                    <div class="mobile-grid-40 grid-parent tablet-grid-35 grid-30">
                        <a href="" class="btn btn-banner">Saiba mais</a>
                    </div>
                </section>
            </div>
            <section class="menu-posts cf">
                <div class="grid-container">
                    <div class="grid-100 grid-container">
                        <h2>Outros fundos de investimentos:</h2>
                        <ul>
                            <li><a href="">Fundos de renda fixa</a></li>
                            <li><a href="">Fundos de ação</a></li>
                            <li><a href="">Fundos de imobiliários</a></li>
                            <li><a href="">Fundos Multimercado</a></li>
                            <li><a href="">Fundos internacionais</a></li>
                            <li><a href="">Fundos de previdencia</a></li>
                        </ul>
                    </div>

                </div>
            </section>
            <section class="cats cat-single cf">
                <div class="grid-container">
                    <div class="s-cat cf">
                        <div class="mobile-grid-35">
                            <h2>Posts Relacionados:</h2>
                        </div>

                    </div>
                </div>
                <div class="list-post-cat list-post-cat-single">
                    <div>
                        <div class="box-posts">
                            <a href="" title="">
                                <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject">
                                    <a href="" itemprop="contentUrl" data-size="0x0">
                                        <img src="<?php bloginfo('template_url')?>/build/imgs/img-sec.jpg" height="" width="" itemprop="thumbnail" alt="Image description" />
                                    </a>
                                    <figcaption itemprop="caption description" class="caption">
                                        <a href="" class="post-cat">Como Investir</a>
                                    </figcaption>
                                </figure>
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="data">20 janeiro de 2019</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                            </a>
                        </div>
                    </div>
                    <div>
                        <div class="box-posts">
                            <a href="" title="">
                                <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject">
                                    <a href="" itemprop="contentUrl" data-size="0x0">
                                        <img src="<?php bloginfo('template_url')?>/build/imgs/img-sec.jpg" height="" width="" itemprop="thumbnail" alt="Image description" />
                                    </a>
                                    <figcaption itemprop="caption description" class="caption">
                                        <a href="" class="post-cat">Como Investir</a>
                                    </figcaption>
                                </figure>
                                <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                <span class="data">20 janeiro de 2019</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>
                            </a>
                        </div>
                    </div>
                </div>

            </section>
        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>
</div>

<?php endwhile; endif; ?>
<?php
get_footer();
