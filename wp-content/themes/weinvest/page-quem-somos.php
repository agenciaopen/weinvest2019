<?php 


/**
 * The template name: Página Quem somos
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';  ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna contato quem-somos cf">
    <div class="grid-container">
        <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
            <section class="conteudo">
                <i class="icon-pc"></i>
                <h1>
                    <?php the_title(); ?>
                </h1>
                <?php the_content(); ?>


                <div class="list cf">
                    <i class="icon-diferenciais"></i>
                    <h2 class="title">Conheça nossos diferenciais</h2>
                    <ul>
                        <li class="cf">
                            <div class="mobile-grid-25 tablet-grid-25 grid-10">
                                <i class="icon-celular"></i>
                            </div>
                            <div class="mobile-grid-75 tablet-grid-75 grid-90">
                                <p><strong>
                                        <?php the_field('primeiro_conteudo');?></strong></p>
                            </div>
                        </li>
                        <li class="cf">
                            <div class="mobile-grid-25 tablet-grid-25 grid-10">
                                <i class="icon-carteira"></i>
                            </div>
                            <div class="mobile-grid-75 tablet-grid-75 grid-90">
                                <p><strong>
                                        <?php the_field('segundo_conteudo');?></strong></p>
                            </div>
                        </li>
                        <li class="cf">
                            <div class="mobile-grid-25  tablet-grid-25 grid-10">
                                <i class="icon-cambio"></i>
                            </div>
                            <div class="mobile-grid-75  tablet-grid-75 grid-90">
                                <p><strong>
                                        <?php the_field('terceiro_conteudo');?></strong></p>
                            </div>
                        </li>

                    </ul>
                </div>

                <div class="list cf politica">
                    <i class="icon-plano"></i>
                    <h2 class="title">Políticas Institucionais </h2>
                    <ul>
                        <li class="cf">
                            <div class="">
                                <h3>
                                    Missão
                                </h3>
                                <p>
                                    <?php the_field('missao');?>
                                </p>
                            </div>
                        </li>
                        <li class="cf">
                            <div class="">
                                <h3>
                                    Visão
                                </h3>
                                <p>
                                    <?php the_field('visao');?>
                                </p>
                            </div>
                        </li>
                        <li class="cf">
                            <div class="">
                                <h3>
                                    Valores
                                </h3>
                              
                                    <?php the_field('valores');?>
                               
                            </div>
                        </li>
                    </ul>
                </div>


                <div class="list cf investimento">
                    <i class="icon-investimento"></i>
                    <h2 class="title">RIVA Investimentos</h2>
                    <img src="<?php bloginfo('template_url')?>/build/imgs/riva.png" height="" width="" itemprop="thumbnail" alt="Image description" />
                    <ul>
                        <li class="cf">

                            <div class="mobile-grid-100 tablet-grid-100 grid-100">
                                <p><strong>
                                        <?php the_field('primeiro_item');?></strong></p>
                            </div>
                        </li>
                        <li class="cf">

                            <div class="mobile-grid-100 tablet-grid-100 grid-100">
                                <p><strong>
                                        <?php the_field('segundo_item');?></strong></p>
                            </div>
                        </li>
                        <li class="cf">

                            <div class="mobile-grid-100  tablet-grid-100 grid-100">
                                <p><strong>
                                        <?php the_field('terceiro_item');?></strong></p>
                            </div>
                        </li>
                        <li class="cf">

                            <div class="mobile-grid-100  tablet-grid-100 grid-100">
                                <p><strong>
                                        <?php the_field('quarto_item');?></strong></p>

                                <p>
                                    <?php the_field('conteudo_investimentos');?>
                                </p>
                            </div>
                        </li>

                    </ul>
                </div>



                <div class="list cf socios">
                    <i class="icon-socios"></i>
                    <h2 class="title">Principais Sócios</h2>
                    <ul>
                    <?php if( have_rows('conteudo_socios') ): ?>
                        <?php while( have_rows('conteudo_socios') ): the_row(); 

                            // vars
                            $foto = get_sub_field('foto');
                            $size = 'full';
                            $nome = get_sub_field('nome');
                            $cargo = get_sub_field('cargo');
                            $descricao = get_sub_field('descricao');
                            $link = get_sub_field('linkedin');

                            ?>
                        <li class="cf">
                            <div class="mobile-grid-35 tablet-grid-25 grid-15">
                                
                                <img src="<?php echo $foto['url']; ?>" alt="<?php echo $foto['alt'] ?>" />
                            </div>
                            <div class="mobile-grid-65 tablet-grid-75 grid-85">
                                <p class="autor"><strong><?=  $nome ?></strong></p>
                                <p class="cargo"><?=  $cargo ?></p>
                            </div>
                            <div class="grid-100 tablet-grid-100 mobile-grid-100">
                               <?=  $descricao ?>
                                <div class="grid-100">
                                    <a href="<?=$link?>" target="_blank">>> Acesse o linkedin</a>
                                </div>

                            </div>
                        </li>
                        <?php endwhile; ?>

                    </ul>
                    <?php endif; ?>
                </div>

            </section>
            <section class="form form-blue cf">
                <div class="grid-container">
                    <i class="icon-plano"></i>
                    <h2>Gostaria de conversar sobre seus investimentos com um especialista?</h2>
                    <?php echo do_shortcode( '[contact-form-7 id="1216" title="Página de Contato"]' ); ?>


                </div>
            </section>

        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>

</div>

<?php endwhile; endif; ?>
<?php
get_footer();
?>
