<?php 


/**
 * The template name: Página Projeção Financeira
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';  ?>
<?php if (have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna contato simuladores cf">
    <div class="grid-container">
        <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
            <section class="conteudo">
                <i class="icon-plano"></i>
                <h1>
                    <?php the_title(); ?>
                </h1>

                <?php the_content(); ?>

                <section class="form form-blue cf">
                    <div class="grid-container">
                        <h2>Calculadora</h2>
                        <label for="idadeI">Idade Inicial:</label>
                        <input type="text" id="idadeI" name="idadeI" />
                        <label for="Nome">Idade Aposentadoria:</label>
                        <input type="text" id="idadeA" name="idadeA" />
                        <label for="InvesI">Investimento Inicial:</label>
                        <input type="text" id="InvesI" name="InvesI" />
                        <label for="Valor">Investimento Mensal:</label>
                        <input type="text" id="Valor" name="Valor" />
                        <input type="submit" vale="Calcular" class="btn" />


                    </div>
                </section>

                <div class="list cf">
                    <div class="grid-container">
                        <i class="icon-plano"></i>
                        <h2>Resultados</h2>
                        <ul>
                            <li class="cf">
                                <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                    <p class="montante"><strong>Montante no Final: </strong></p>
                                    <span class="cmontante">(liquido de inflação)</span>
                                </div>
                                <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                    <p class="total"><strong>R$530.460,00</strong></p>
                                </div>
                            </li>
                            <li class="cf">
                                <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                    <p class="montante"><strong>Montante no Final: </strong></p>
                                    <span class="cmontante">(liquido de inflação)</span>
                                </div>
                                <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                    <p class="total"><strong>R$530.460,00</strong></p>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

                


            </section>
            <section class="shared cf">
                <div class="grid-container">
                    <div class="social">
                        <div class="grid-container">
                            <h2>Compartilhe: </h2>
                            <ul>
                                <li>
                                    <a href="http://facebook.com/share.php?u=<?php the_permalink();?>&title=<?php the_title();?>" target="_blank" class="link-face" title="Compartilhar <?php the_title();?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;"><i class="icon-facebook"></i></a>

                                </li>
                                <li>
                                    <i class="icon-twitter"></i>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>

            </section>
            
            
        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>

</div>

<?php endwhile; endif; ?>
<?php
get_footer();
?>
