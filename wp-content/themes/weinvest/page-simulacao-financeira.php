<?php 

/**
 * The template name: Página Projeção Financeira
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';  
?>
<?php if (have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna contato simuladores cf">
    <div class="grid-container">
        <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
            <section class="conteudo">
                <i class="icon-plano"></i>
                <h1>
                    <?php the_title(); ?>
                </h1>

                <?php the_content(); ?>

                <section class="form form-blue cf">
                    <div class="grid-container">
                        <?php  $valorPorcentagem = get_field('valor'); ?>
                         <?php  $valorPorcentagem2 = get_field('valor2'); ?>
                        <h2>Calculadora</h2>
                        <label for="idade">Idade Inicial:</label>
                        <input type="text" name="idadeInicial" id="idInicial" class='numbers' maxlength="3" required>

                        <label for="idadeAposentadoria">Idade Aposentadoria:</label>
                        <input type="text" name="idadeAposentadoria" id="idadeAposentadoria" class='numbers' maxlength="3">

                        <label for="valorInicial">Investimento Inicial (R$):</label>
                        <input class="dinheiro" type="text" name="valorInicial" id="invInicial" class='numbers'>

                        <label for="valorMensal">Investimento Mensal (R$):</label>
                        <input class="dinheiro" type="text" name="valorMensal" id="invMensal" class='numbers'>
                        
                         <input type="hidden" name="" id="valorPorcentagem"  value="<?= $valorPorcentagem  ?>">
                         <input type="hidden" name="" id="valorPorcentagem2"  value="<?= $valorPorcentagem2  ?>">

                        <button type="button" class="btn" id="btnSimulaPrevidencia">Calcular</button>

                    </div>
                </section>

                <div id="resultado" class="list cf resultado">
                    <div class="grid-container">
                        <i class="icon-plano"></i>
                        <h2>Resultados</h2>
                        <ul>
                            <li class="cf">
                                <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                    <p class="montante"><strong>Montante no Final: </strong></p>
                                    <span class="cmontante">(líquido de inflação)</span>
                                </div>
                                <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                    <p class="total"><strong>R$ <span class="montante-final"></span> </strong></p>
                                </div>
                            </li>
                            <li class="cf">
                                <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                    <p class="montante"><strong>Rendimento Mensal: </strong></p>
                                    <span class="cmontante">(líquido de inflação)</span>
                                </div>
                                <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                    <p class="total"><strong>R$ <span class="rendimento"></span> </strong></p>
                                </div>
                            </li>

                        </ul>
                    </div>
                  <div class="grid-100 tablet-grid-100">
                        <br /><br />
                        <h2> Entenda o resultado desta simulação:</h2>
                       <p> <?php the_field('texto_projecao_financeira'); ?></p>

                    </div>
                    <div class="grid-100 tablet-grid-100">
                        <div class="conteudo-tooltip">
                           <a href="" target="_blank"><?php get_field('texto_botao_saiba'); ?></a>
                        </div>

                    </div>
                </div>


            </section>
            <section class="shared cf">
                <div class="grid-container">
                    <div class="social">
                        <div class="grid-container">
                            <ul>
                                <li>
                                    <p> COMPARTILHE:</p>
                                </li>
                                <li>
                                    <a href="http://facebook.com/share.php?u=<?= $url; ?>&title=<?= get_the_title(); ?>" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                        <i class="icon-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/intent/tweet?url=<?= $url; ?>&text=<?= get_the_title(); ?>&via=WeInvestNet" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Twitter" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                        <i class="icon-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://api.whatsapp.com/send?text=Veja esta página do site da WeInvest: <?= $url; ?>" class="link-face" title="Compartilhar <?= get_the_title(); ?> no whatsapp">
                                        <i class="icon-whatsapp"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>


        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>

</div>

<?php endwhile; endif; ?>
<?php
get_footer();
?>

<script>
    $(document).on('click', "#btnSimulaPrevidencia", function() {

        // pegando os dados
        var idade = $('#idInicial').val();
        var idadeAposentadoria = $('#idadeAposentadoria').val();
        var valorInical = $('#invInicial').val().replace(/[^\d]+/g, '');
        var valorMensal = $('#invMensal').val().replace(/[^\d]+/g, '');
        var valorPorcentagem = $('#valorPorcentagem').val();
        var valorPorcentagem2 = $('#valorPorcentagem2').val();
        if (idade == "") {
            $("#idInicial").addClass('error');
            $("#idInicial").focus();
            return false;
        } else if (idadeAposentadoria == "") {
            $("#idadeAposentadoria").addClass('error');
            $("#idadeAposentadoria").focus();
            return false;
        } else if (valorInical == "") {
            $("#invInicial").addClass('error');
            $("#invInicial").focus();
            return false;
        } else if (valorMensal == "") {
            $("#invMensal").addClass('error');
            $("#invMensal").focus();
            return false;
        } else {
            $("#idInicial").removeClass('error');
            $("#idadeAposentadoria").removeClass('error');
            $("#invInicial").removeClass('error');
            $("#invMensal").removeClass('error');

            loadIn();
        }

        // criando as variáveis
        var vUrl = "<?= get_template_directory_uri(); ?>" + "/get-projecao.php";
        var vData = {
            idadeInicial: idade,
            idadeAposentadoria: idadeAposentadoria,
            valorInicial: valorInical,
            valorMensal: valorMensal,
            valorPorcentagem : valorPorcentagem,
             valorPorcentagem2 : valorPorcentagem2
        };

        $.ajax({
            type: "POST",
            url: vUrl,
            data: vData,
            success: function(dataValor, status) {
                $('html, body').animate({
                    scrollTop: $('#resultado').offset().top
                }, 800);
                loadOut();
                $('.resultado').fadeIn();

                var JSONObject = JSON.parse(dataValor);

                $('.resultado .montante-final').html(JSONObject.montante);
                $('.resultado .rendimento').html(JSONObject.rendimento);

            }
        });
    });

</script>
