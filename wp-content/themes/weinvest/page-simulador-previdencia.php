<?php 


/**
 * The template name: Página Simulador Previdencia
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';  

?>
<?php if (have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna contato simuladores cf">
    <div class="grid-container">
        <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
            <section class="conteudo">
                <i class="icon-plano"></i>
                <h1>
                    <?php the_title(); ?>
                </h1>

                <?php the_content(); ?>

                <section class="form form-blue cf">
                    <div class="grid-container">
                        <h2>Comparador</h2>

                        <label for="cnpj">CNPJ do fundo que deseja comparar:</label>
                        <input type="text" name="cnpj" id="cnpj" class='cnpj' required>
                        <label for="cnpj">Ou nome do fundo que deseja comparar:</label>
                        <input type="text" id="fundos" />


                        <button type="button" class="btn" id="btnSimulaPrevidencia"> Buscar </button>




                    </div>
                </section>
                <div id="error-cpf"></div>
                <div id="resultadoPrevidencia" class="list simulador-previdencia cf">
                    <div class="grid-container">
                        <i class="icon-plano"></i>
                        <h2>Resultados para o fundo : <span class="nomeFundo"></span></h2>

                        <div class="mobile-grid-100 tablet-grid-50 grid-50 text-center">
                            <p> Mês </p>
                            <div id="columnchart_values-m"></div>

                        </div>

                        <div class="mobile-grid-100 tablet-grid-50 grid-50 text-center">
                            <p> 12 meses </p>
                            <div id="columnchart_values"></div>

                        </div>

                        <div class="mobile-grid-100 tablet-grid-50 grid-50 text-center">
                            <p> 24 Meses </p>
                            <div id="columnchart_values-24"></div>

                        </div>

                        <div class="mobile-grid-100 tablet-grid-50 grid-50 text-center">
                            <p> 36 Meses </p>
                            <div id="columnchart_values-36"></div>

                        </div>
                        <!-- <ul>
                            <li class="cf">
                                <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                    <p class="montante"><strong>Montante no Final: </strong></p>
                                    <span class="cmontante">(liquido de inflação)</span>
                                </div>
                                <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                    <p class="total"><strong>R$530.460,00</strong></p>
                                </div>
                            </li>
                            <li class="cf">
                                <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                    <p class="montante"><strong>Montante no Final: </strong></p>
                                    <span class="cmontante">(liquido de inflação)</span>
                                </div>
                                <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                    <p class="total"><strong>R$530.460,00</strong></p>
                                </div>
                            </li>

                        </ul> -->
                    </div>
                    <div class="grid-100 tablet-grid-100">
                        <br /><br />
                        <h2> Entenda o resultado desta simulação:</h2>
                        <p>
                            <?php the_field('texto_simulacao_comparador_de_previdencia'); ?>
                        </p>

                    </div>
                    <div class="grid-100 tablet-grid-100">
                        <div class="conteudo-tooltip">
                            <a href="<?php the_field('link'); ?>" target="_blank"><?php the_field('texto_botao_saiba'); ?></a>
                        </div>

                    </div>
                </div>
            </section>

            <section class="shared cf">
                <div class="grid-container">
                    <div class="social">
                        <div class="grid-container">
                            <ul>
                                <li>
                                    <p> COMPARTILHE:</p>
                                </li>
                                <li>
                                    <a href="http://facebook.com/share.php?u=<?= $url; ?>&title=<?= get_the_title(); ?>" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                        <i class="icon-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/intent/tweet?url=<?= $url; ?>&text=<?= get_the_title(); ?>&via=WeInvestNet" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Twitter" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                        <i class="icon-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://api.whatsapp.com/send?text=Veja esta página do site da WeInvest: <?= $url; ?>" class="link-face" title="Compartilhar <?= get_the_title(); ?> no whatsapp">
                                        <i class="icon-whatsapp"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>

</div>

<?php endwhile; endif; ?>
<?php
get_footer();

include 'get-fundos.php';
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
    var cnpjEscolhido = '';
    $(function() {
        var fundos = <?php  echo json_encode($mercado) ?>;
        $("#fundos").autocomplete({
            source: fundos,
            select: function(e, ui) {
                e.preventDefault();
                cnpjEscolhido = ui.item.value;
                $(this).val(ui.item.label);
            }
        });
    });

    $(document).on('blur', "#cnpj", function() {
        if ($(this).val() != '') {
            $('#fundos').val('');
        }
    });
    $(document).on('blur', "#fundos", function() {
        if ($(this).val() != '') {
            $('#cnpj').val('');
        }
    });

    $(document).on('click', "#btnSimulaPrevidencia", function() {

        // pegando os dados
        var vCnpj = $("#cnpj").val().replace(/[^\d]+/g, '');


        if ((cnpj == "") && (cnpjEscolhido == "")) {
            $("#cnpj,#fundos").addClass('error');
            $("#cnpj,#fundos").focus();
            return false;
        } else {
            $("#cnpj").removeClass('error');
            if (vCnpj == "") {
                vCnpj = cnpjEscolhido;

            }
            loadIn();
        }

        // criando as variáveis
        var vUrl = "<?= get_template_directory_uri(); ?>" + "/get-previdencia.php";
        var vData = {
            cnpj: vCnpj
        };




        $.ajax({
            type: "POST",
            url: vUrl,
            data: vData,
            success: function(dataValor, status) {
                if (dataValor != '') {
                    $('.simulador-previdencia').slideDown();



                    // pegando os dados jSON
                    var obj = jQuery.parseJSON(dataValor);

                    var fundoInserido = obj[0]['fundo'];
                    var fundoSugerido = obj[1]['fundo'];
                    $(".nomeFundo").text(fundoInserido);

                    var fundoInserido_meses = obj[0]['meses'].replace("%", "");
                    var fundoSugerido_meses = obj[1]['meses'].replace("%", "");

                    var fundoInserido_12meses = obj[0]['12meses'].replace("%", "");
                    var fundoSugerido_12meses = obj[1]['12meses'].replace("%", "");

                    var fundoInserido_24meses = obj[0]['24meses'].replace("%", "");
                    var fundoSugerido_24meses = obj[1]['24meses'].replace("%", "");

                    var fundoInserido_36meses = obj[0]['36meses'].replace("%", "");
                    var fundoSugerido_36meses = obj[1]['36meses'].replace("%", "");

                    google.charts.load("current", {
                        packages: ['corechart']
                    });

                    google.charts.setOnLoadCallback(drawChartm);
                    google.charts.setOnLoadCallback(drawChart);
                    google.charts.setOnLoadCallback(drawChart24);
                    google.charts.setOnLoadCallback(drawChart36);

                    function drawChartm() {
                        var data = google.visualization.arrayToDataTable([
                            ["Fundo", "Valor Percentual do CDI", {
                                role: "style"
                            }],
                            [fundoInserido, parseFloat(fundoInserido_meses), "color: #cccccc"],
                            [fundoSugerido, parseFloat(fundoSugerido_meses), "color: #30718c"]
                        ]);

                        var view = new google.visualization.DataView(data);
                        view.setColumns([0, 1,
                            {
                                calc: "stringify",
                                sourceColumn: 1,
                                type: "string",
                                role: "annotation"
                            },
                            2
                        ]);

                        var options = {

                            height: 400,
                            legend: {
                                position: "none"
                            },
                            vAxis: {
                                title: "Valores em % do CDI",
                                viewWindow: {
                                    min: 0
                                }
                            }
                        };
                        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values-m"));
                        chart.draw(view, options);
                    }

                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ["Fundo", "Valor percentual do CDI", {
                                role: "style"
                            }],
                            [fundoInserido, parseFloat(fundoInserido_12meses), "color: #cccccc"],
                            [fundoSugerido, parseFloat(fundoSugerido_12meses), "color: #30718c"]
                        ]);

                        var view = new google.visualization.DataView(data);
                        view.setColumns([0, 1,
                            {
                                calc: "stringify",
                                sourceColumn: 1,
                                type: "string",
                                role: "annotation"
                            },
                            2
                        ]);

                        var options = {

                            height: 400,
                            legend: {
                                position: "none"
                            },
                            vAxis: {
                                title: "Valores em % do CDI",
                                viewWindow: {
                                    min: 0
                                }
                            }
                        };
                        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
                        chart.draw(view, options);
                    }

                    function drawChart24() {
                        var data = google.visualization.arrayToDataTable([
                            ["Fundo", "Valor percentual do CDI", {
                                role: "style"
                            }],
                            [fundoInserido, parseFloat(fundoInserido_24meses), 'color: #cccccc'],
                            [fundoSugerido, parseFloat(fundoSugerido_24meses), 'color: #30718c']
                        ]);

                        var view = new google.visualization.DataView(data);
                        view.setColumns([0, 1,
                            {
                                calc: "stringify",
                                sourceColumn: 1,
                                type: "string",
                                role: "annotation"
                            },
                            2
                        ]);

                        var options = {

                            height: 400,
                            legend: {
                                position: "none"
                            },
                            vAxis: {
                                title: "Valores em % do CDI",
                                viewWindow: {
                                    min: 0
                                }
                            }
                        };
                        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values-24"));
                        chart.draw(view, options);
                    }

                    function drawChart36() {
                        var data = google.visualization.arrayToDataTable([
                            ["Fundo", "Valor percentual do CDI", {
                                role: "style"
                            }],
                            [fundoInserido, parseFloat(fundoInserido_36meses), 'color: #cccccc'],
                            [fundoSugerido, parseFloat(fundoSugerido_36meses), 'color: #30718c']
                        ]);

                        var view = new google.visualization.DataView(data);
                        view.setColumns([0, 1,
                            {
                                calc: "stringify",
                                sourceColumn: 1,
                                type: "string",
                                role: "annotation"
                            },
                            2
                        ]);

                        var options = {

                            height: 400,
                            legend: {
                                position: "none"
                            },
                            vAxis: {
                                title: "Valores em % do CDI",
                                viewWindow: {
                                    min: 0
                                }
                            }
                        };
                        var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values-36"));
                        chart.draw(view, options);
                    }

                } else {


                    $("#error-cpf").html("<h3 class='title'>O CNPJ digitado é inválido ou não existe na nossa base de dados! </h3>");

                }
                $('html, body').animate({
                    scrollTop: $('#resultadoPrevidencia').offset().top
                }, 800);


                loadOut();
            }
        });
    });

</script>
