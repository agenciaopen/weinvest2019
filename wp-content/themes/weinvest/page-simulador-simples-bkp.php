<?php


/**
 * The template name: Página Simulador Simples
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';

require 'spreadsheet/vendor/autoload.php';

function acrescimo($valor, $porcentagem, $potencia)
{

    $fator = (1 + ($porcentagem / 100));
    $val_potencia = pow($fator, $potencia);
    $resultado = $valor * $val_potencia;

    $resultado = round($resultado, 2);

    return $resultado;
}

function getPremissa($campo)
{
    $caminho_planilha = './json/planilha.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($caminho_planilha);

    $val_inv = $spreadsheet->getActiveSheet()->getCell($campo)->getFormattedValue();
    $val = str_replace("%", "", $val_inv);
    return $val;
}

function getValores($val, $prazo, $premissa)
{
    $valor_sugerido = $val;
    $arr = array();

    for ($i = 1; $i <= $prazo; $i++) {
        array_push($arr, $valor_sugerido);
        $valor_sugerido = acrescimo($val, $premissa, $i + 1);
    }

    return $arr;
}

function concatValores($prazo, $lista1, $lista2, $lista3)
{
    $valores = array();

    for ($i = 0; $i < $prazo; $i++) {
        array_push(
            $valores,
            array("inv" => $lista1[$i], "cdi" => $lista2[$i], "poup" => $lista3[$i])
        );
    }

    return $valores;
}

function getJson($val)
{
    //apaga o arquivo existente para gerar um com novos dados
    array_map('unlink', glob("./json/valores.json"));

    // Tranforma o array $dados_identificador em JSON
    $dados_json = json_encode($val);

    // Abre ou cria o arquivo contato.json
    // "a" indicar que o arquivo é aberto para ser escrito
    $fp = fopen("./json/valores.json", "a");

    // Escreve o conteúdo JSON no arquivo
    $escreve = fwrite($fp, $dados_json);

    // Fecha o arqui
    fclose($fp);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    require 'spreadsheet/vendor/autoload.php';

    global $spreadsheet;
        
    $caminho_planilha = './json/planilha.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($caminho_planilha);

    $val_cdi = $spreadsheet->getActiveSheet()->getCell('I12')->getFormattedValue();
    $val_poupanca = $spreadsheet->getActiveSheet()->getCell('I11')->getFormattedValue();

    $tableReferencePremissa = [
        "conservador" => [
            6   => "G4",
            12  => "H4",
            24  => "I4",
            36  => "J4",
            60  => "K4",
            96  => "L4",
            12 => "M4",
            180 => "N4",
        ],
        "conservador-moderado" => [
            "6"   => "G5",
            "12"  => "H5",
            "24"  => "I5",
            "36"  => "J5",
            "60"  => "K5",
            "96"  => "L5",
            "120" => "M5",
            "180" => "N5",
        ],
        "moderado" => [
            "6"   => "G6",
            "12"  => "H6",
            "24"  => "I6",
            "36"  => "J6",
            "60"  => "K6",
            "96"  => "L6",
            "120" => "M6",
            "180" => "N6",
        ],
        "agressivo" => [
            "6"   => "G7",
            "12"  => "H7",
            "24"  => "I7",
            "36"  => "J7",
            "60"  => "K7",
            "96"  => "L7",
            "120" => "M7",
            "180" => "N7",
        ],
    ];

    $valor_de_entrada = 0;
    $meses = "";
    $perfil = "";

    

    if (!empty($_POST["valor"])) {
        $valor_de_entrada = (int)$_POST["valor"];
    }
    if (!empty($_POST["prazo"])) {
        $meses = (int)$_POST["prazo"];
    }
    if (!empty($_POST["perfil"])) {
        $perfil = (string)$_POST["perfil"];
    }

    $premissa = getPremissa($tableReferencePremissa[$perfil][$meses]);

    $inv_sugerido = getValores($valor_de_entrada, $meses, $premissa);

    $cdi = getValores($valor_de_entrada, $meses, $val_cdi);

    $poupanca = getValores($valor_de_entrada, $meses, $val_poupanca);

    $valores = concatValores( $meses, $inv_sugerido, $cdi, $poupanca);

    getJson($valores);

} else {
    //Se não tiver post apaga o arquivo json para não ter leitura desnecessaria 
    array_map('unlink', glob("./json/valores.json"));
}

?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="page-interna contato simuladores cf">
            <div class="grid-container">
                <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
                    <section class="conteudo">
                        <i class="icon-plano"></i>
                        <h1>
                            <?php the_title(); ?>
                        </h1>

                        <?php the_content(); ?>

                        <section class="form form-blue cf">
                            <div class="grid-container">
                                <h2>Calculadora</h2>

                                <form action="<?php bloginfo('url'); ?>/ferramenta-de-simulador-simples/" method="post">
                                    <label for="valor">Valor:</label>
                                    <input type="text" id="valor" name="valor" required value="<?= ($_SERVER["REQUEST_METHOD"] == "POST") ? $_POST['valor'] : ""  ?>" />
                                    <label for="prazo">Prazo:</label>
                                    <select name="prazo" id="" required>
                                        <option value=""> Selecione o prazo </option>
                                        <option value="6" <?= ($meses == 6)   ? 'selected' : '' ?>> 6 </option>
                                        <option value="12" <?= ($meses == 12)  ? 'selected' : '' ?>> 12 </option>
                                        <option value="24" <?= ($meses == 24)  ? 'selected' : '' ?>> 24 </option>
                                        <option value="36" <?= ($meses == 36)  ? 'selected' : '' ?>> 36 </option>
                                        <option value="60" <?= ($meses == 60)  ? 'selected' : '' ?>> 60 </option>
                                        <option value="96" <?= ($meses == 96)  ? 'selected' : '' ?>> 96 </option>
                                        <option value="120" <?= ($meses == 120) ? 'selected' : '' ?>> 120 </option>
                                        <option value="180" <?= ($meses == 180) ? 'selected' : '' ?>> 180 </option>
                                    </select>
                                    <label for="perfil">Perfil do Investidor:</label>
                                    <select name="perfil" id="" required><br>
                                        <option value=""> Selecione o perfil </option>
                                        <option value="conservador" <?= ($perfil == 'conservador') ? 'selected' : '' ?>> Conservador </option>
                                        <option value="conservador-moderado" <?= ($perfil == 'conservador-moderado') ? 'selected' : '' ?>> Consevador Moderado </option>
                                        <option value="moderado" <?= ($perfil == 'moderado') ? 'selected' : '' ?>> Moderado </option>
                                        <option value="agressivo" <?= ($perfil == 'agressivo') ? 'selected' : '' ?>> Agressivo </option>
                                    </select>
                                    <input type="submit" vale="Calcular" class="btn" />
                                </form>
                            </div>
                            
                        </section>

                        <div class="list cf">
                            <div class="grid-container">

                                <i class="icon-plano"></i>
                                <h2>Comparador</h2>

                                <div id="curve_chart" style="width: 100%; height: 400px;"></div>

                                <ul class="none">
                                    <li class="cf">
                                        <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                            <p class="montante"><strong>Montante no Final: </strong></p>
                                            <span class="cmontante">(liquido de inflação)</span>
                                        </div>
                                        <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                            <p class="total"><strong>R$530.460,00</strong></p>
                                        </div>
                                    </li>
                                    <li class="cf">
                                        <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                            <p class="montante"><strong>Montante no Final: </strong></p>
                                            <span class="cmontante">(liquido de inflação)</span>
                                        </div>
                                        <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                            <p class="total"><strong>R$530.460,00</strong></p>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>

                    </section>

                    <section class="shared cf">
                        <div class="grid-container">
                            <div class="social">
                                <div class="grid-container">
                                    <h2>Compartilhe: </h2>
                                    <ul>
                                        <li>
                                            <a href="http://facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" class="link-face" title="Compartilhar <?php the_title(); ?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;"><i class="icon-facebook"></i></a>
                                        </li>
                                        <li>
                                            <i class="icon-twitter"></i>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>

                    </section>

                </div>

                <!-- Sidebar  -->
                <?php get_sidebar(); ?>
                <!-- /Sidebar  -->
                
            </div>

        </div>

    <?php endwhile;
endif; ?>
<?php
get_footer();
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

    $(document).ready(function(){
        $.get("../json/valores.json", function(dataValor) {

            console.table(dataValor);
            
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = new google.visualization.DataTable();
                
                data.addColumn( 'number', 'Mês' ); 
                data.addColumn( 'number', 'Inv. Sugerido' );  
                data.addColumn( 'number', 'CDI' );  
                data.addColumn( 'number', 'Poupança' );
                

                for (var n= 0; n < dataValor.length ;n++){
                    data.addRow([ 
                        n+1, 
                        parseFloat(dataValor[n]["inv"]),
                        parseFloat(dataValor[n]["cdi"]),
                        parseFloat(dataValor[n]["poup"])
                    ]);
                }   

                var options = {
                    curveType: 'function',
                    legend: { position: 'bottom' },
                    vAxis: {
                        title: 'Reais ( R$ )'
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

                chart.draw(data, options);
            }
        });
    });
</script>