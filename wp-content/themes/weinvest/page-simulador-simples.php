<?php
/**
 * The template name: Página Simulador Simples
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        We Invest
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';

?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="page-interna contato simuladores cf">
            <div class="grid-container">
                <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">
                    <section class="conteudo">
                        <i class="icon-plano"></i>
                        <h1>
                            <?php the_title(); ?>
                        </h1>

                        <?php the_content(); ?>

                        <section class="form form-blue cf">
                            <div class="grid-container">
                                <h2>Calculadora</h2>
                                <label for="valor">Valor disponível para investir (R$):</label>
                                <input type="text" id="valor" name="valor" class="dinheiro"  required />
                                <label for="prazo">Prazo (meses):</label>
                                <select name="prazo" id="prazo" required>
                                    <option >Selecionar Prazo </option>
                                    <option value="6"> 6 </option>
                                    <option value="12"> 12 </option>
                                    <option value="24"> 24 </option>
                                    <option value="36"> 36 </option>
                                    <option value="60"> 60 </option>
                                    <option value="96"> 96 </option>
                                    <option value="120"> 120 </option>
                                    <option value="180"> 180 </option>
                                </select>
                                <label for="perfil">Perfil do Investidor:</label>
                                <select name="perfil" id="perfil" required><br>
                                      <option >Selecionar Perfil do Investidor </option>
                                    <option value="conservador"> Conservador </option>
                                    <option value="conservador-moderado"> Consevador Moderado </option>
                                    <option value="moderado"> Moderado </option>
                                    <option value="agressivo"> Agressivo </option>
                                </select>
                                <button type="button" class="btn" id="btnEnviarDados">Calcular</button>
                            </div>

                        </section>

                        <div class="list cf comparador" id="comparador">
                            <div class="grid-container">

                                <i class="icon-plano"></i>
                                <h2>Comparador</h2>

                                <div id="curve_chart" style="width: 100%; height: 400px;"></div>

                                <ul >
                                     <li class="cf">
                                        <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                            <p class="montante"><strong> Acumulado do investimento <br />sugerido no período: </strong></p>

                                        </div>
                                        <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                                <p class="total"><strong>R$ <span class="rendimento" id="totalRendimentos"></span> </strong></p>
                                        </div>
                                    </li>
                                     <li class="cf">
                                        <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                            <p class="montante"><strong>Acumulado na Poupança: </strong></p>

                                        </div>
                                        <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                                <p class="total"><strong>R$ <span class="rendimento" id="totalRendimentop"></span> </strong></p>
                                        </div>
                                    </li>
                                    <li class="cf">
                                        <div class="mobile-grid-100 tablet-grid-40 grid-40 grid-parent">
                                            <p class="montante"><strong> Diferença: </strong></p>
                                            <span class="cmontante">(Investimento Sugerido x Poupança)</span>
                                        </div>
                                        <div class="mobile-grid-100 tablet-grid-60 grid-60 grid-parent">
                                                <p class="total"><strong>R$ <span class="rendimento" id="totalRendimento"></span> </strong></p>
                                        </div>
                                    </li>



                                </ul>
                            </div>

                      <div class="grid-100 tablet-grid-100">
                        <br /><br />
                        <h2> Entenda o resultado desta simulação:</h2>
                        <p> <?php the_field('texto_simulador_simples'); ?></p>

                    </div>
                    <div class="grid-100 tablet-grid-100">
                        <div class="conteudo-tooltip">
                           <a href="<?php the_field('link'); ?>" target="_blank"><?php the_field('texto_botao_saiba'); ?></a>
                        </div>

                    </div>
                        </div>



                    </section>

                    <section class="shared cf">
                        <div class="grid-container">
                            <div class="social">
                                <div class="grid-container">
                                    <ul>
                                        <li>
                                            <p> COMPARTILHE:</p>
                                        </li>
                                        <li>
                                            <a
                                                href="http://facebook.com/share.php?u=<?= $url; ?>&title=<?= get_the_title(); ?>"
                                                target="_blank"
                                                class="link-face"
                                                title="Compartilhar <?= get_the_title(); ?> no Facebook"
                                                onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                                    <i class="icon-facebook"></i>
                                                </a>
                                        </li>
                                        <li>
                                            <a
                                                href="https://twitter.com/intent/tweet?url=<?= $url; ?>&text=<?= get_the_title(); ?>&via=WeInvestNet"
                                                target="_blank"
                                                class="link-face"
                                                title="Compartilhar <?= get_the_title(); ?> no Twitter"
                                                onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                                    <i class="icon-twitter"></i>
                                                </a>
                                        </li>
                                        <li>
                                            <a
                                                href="https://api.whatsapp.com/send?text=Veja esta página do site da WeInvest: <?= $url; ?>"
                                                class="link-face"
                                                title="Compartilhar <?= get_the_title(); ?> no whatsapp" >
                                                    <i class="icon-whatsapp"></i>
                                                </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>

                <!-- Sidebar  -->
                <?php get_sidebar(); ?>
                <!-- /Sidebar  -->

            </div>

        </div>

    <?php endwhile;
endif; ?>
<?php
get_footer();
?>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

$(document).on('click', "#btnEnviarDados", function () {

    // pegando os dados
    var vValor = $("#valor").val().replace(/[^\d]+/g,'');
    vValor  = vValor.substr(0, vValor.length -2);
    console.log(vValor);
    var vPrazo = $("#prazo").val();
    var vPerfil = $("#perfil").val();

    var erro = 0;

    if( vValor == ""){
        $("#valor").addClass('error');
        return false;
    }else{
        $("#valor").removeClass('error');
        loadIn();
    }

    // criando as variáveis
    var vUrl = "https://weinvest.agenciaopen.com.br/teste/";
    var vData = { valor:vValor, prazo:vPrazo, perfil: vPerfil };

    $.ajax({
        type: "POST",
        url: vUrl,
        data: vData,
        success: function(dataValor,status){

            $('.comparador').slideDown();

            $('html, body').animate({
                scrollTop: $( '#comparador' ).offset().top
            }, 800);

            loadOut();

            // pegando os dados jSON
            var obj = jQuery.parseJSON(dataValor);

            google.charts.load('current', {'packages':['corechart'],'language': 'pt'});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = new google.visualization.DataTable();

                data.addColumn( 'number', 'Mês' );
                data.addColumn( 'number', 'Inv. Sug' );
                data.addColumn( 'number', 'CDI' );
                data.addColumn( 'number', 'Poup' );


                for (var n= 0; n < obj.length ;n++){
                    data.addRow([
                        n+1,
                        parseFloat(obj[n]["inv"]),
                        parseFloat(obj[n]["cdi"]),
                        parseFloat(obj[n]["poup"])
                    ]);
                }

                var options = {
                    curveType: 'function',
                    legend: { position: 'bottom' },
                    vAxis: {
                        title: 'Reais ( R$ )'
                    }
                };



                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

                chart.draw(data, options);
            }

            var valor1 = 0;
            var valor2 = 0;
            for (var n= 0; n < obj.length ;n++){


               valor1 = parseFloat(obj[n]["inv"]);
               valor2 = parseFloat(obj[n]["poup"]);


            }
            console.log( valor1);
            console.log( valor2);
            var total = valor1 - valor2 ;


              $('#totalRendimentos').text(valor1.toLocaleString('pt-BR'));
              $('#totalRendimentop').text(valor2.toLocaleString('pt-BR'));
              $('#totalRendimento').text(total.toLocaleString('pt-BR'));

        }
    });
});
</script>
