<?php 


/**
 * The template name: Página Sucesso
 * @package   WordPress
 * @subpackage   agenciaopen
 * @since    2017
 * @author:         Arlen Resende
 * Projeto:        COntrutora passos
 * Data de Criação: 04/01/2018
 * Version:   1.0
 */

include_once 'header.php';  ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-interna sucesso">
    <section class="vitrine-interna">
        <div class="grid-container">
            <div class="content-vitrine cf">

                <div class="grid-30 content_sucesso">
                     <img src="<?php bloginfo('template_url')?>/build/imgs/letter.jpg'" alt="">
                    <h1 class="title">
                       
                       Mensagem enviada
                    </h1>
                    <p>Sua mensagem foi recebida com sucesso, <br />entraremos em contato com você em breve!</p>
                    <a href="<?= get_site_url(); ?>" title="Voltar para a página inicial" class="voltar">Voltar para a página inicial</a>
                </div>




            </div>
        </div>

    </section>
   
</div>

<?php endwhile; endif; ?>
<?php
get_footer();
