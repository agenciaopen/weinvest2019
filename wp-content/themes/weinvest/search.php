<?php
    /**
        * Template Name: Search
    */

    get_header(); 
    $search = get_search_query( false );
?>

<div class="page-interna page-list pg-single cf">
    <div class="grid-container grid-parent">
        <div class="tablet-grid-60 tablet-prefix-20 tablet-suffix-20 grid-70 prefix-15 suffix-30 hide-on-desktop">
            <section class="box-banner cf">
                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                            'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();

                                        $imageBannerm = get_field('imagem-desk');
                                        $size = 'full';
                                        if (!empty($imageBannerm)) : ?>
                                            <a href="<?= get_field('link'); ?>">
                                                <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                            </a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>

                            <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                                <section class="banner cf">
                                    <?php
                                    $bannerm = new WP_Query(
                                        array(
                                          'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();
                                        $imageBannerm = get_field('imagem_celular');
                                        $size = 'full';

                                        if (!empty($imageBannerm)) : ?>
                                            <a href="<?= get_field('link'); ?>">
                                                <img src="<?= $imageBannerm['url']; ?>" alt="" />
                                            </a>
                                        <?php endif; ?>
                                    <?php endwhile; ?>
                                </section>
                            </div>
                        </section>
        </div>
        <!-- Main  -->
        <div class="tablet-grid-70 grid-parent grid-70 suffix-5">
            <section class="brands">

                <div class="grid-container">
                    <p id="breadcrumbs">
                        <span>
                            <span>
                                <a href="//localhost:3000/site-weinvest/">Home</a> 
                                » Resultado da busca por:
                                <strong class="breadcrumb_last"> <?= $search; ?></strong>
                            </span>
                        </span
                    ></p>
                </div>

            </section>
            <section class="cats  cf">
                <?php
                    // WP_Query arguments
                    $args = array(
                        's'              => $search,
                        'posts_per_page' => 20
                    );

                    // The Query
                    $busca = new WP_Query( $args );

                    // The Loop
                    if ( $busca->have_posts() ) {
                        while ( $busca->have_posts() ) {
                            $busca->the_post(); 
                            
                            $category = get_the_category();
                            $catId = $category[0]->cat_ID;

                            $catParent = $category[0]->parent;
                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                $catId = $catPai->cat_ID;
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catName = $category[0]->name;
                                $catSlug = $category[0]->slug;
                            }
                        ?>
                            <div class="tablet-grid-50 grid-50 the_post">
                                <div class="box-list-cats box-posts setCookie" data-cat="<?= $catId; ?>">
                                    <a href="<?php the_permalink(); ?>" title="">
                                        <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>')">
                                            <figcaption itemprop="caption description" class="caption">
                                                <span class="post-cat <?= $catSlug; ?>">
                                                    <?= $catName; ?>
                                                </span>
                                            </figcaption>
                                        </figure>
                                        <h2 class="<?= $catSlug; ?>">
                                            <?= title_limite(100); ?>
                                        </h2>
                                        <span class="data">
                                            <?= get_the_date(); ?></span>
                                        <p>
                                            <?php wp_limit_post(150,'...',true); ?>
                                        </p>
                                    </a>
                                </div>
                            </div>

                    <?php    }
                    }else{ 
                ?>
                    <div class="grid-container">
                        <h2> Sua busca não encontrou nenhum resultado! </h2>        
                    </div>
                <?php
                    } 
                    // Restore original Post Data
                    wp_reset_postdata();
                ?>

            </section>
            <?php require 'box-post-populares.php';?>
        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>

</div>

<?php get_footer(); ?>
