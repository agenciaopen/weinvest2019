<?php
	/**
		** Template Name: Search Form
	**/
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" id="searchform">
	<label for="s">
		<input type="search" name="s" id="s" value="<?= get_search_query(); ?>" class="align-middle input_txt rounded" placeholder="O que você procura?" required />
		<button type="submit" class="btn-buscar"><i class="icon-lupa"></i> </button>
	</label>
</form>