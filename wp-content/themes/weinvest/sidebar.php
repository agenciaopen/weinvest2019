<aside class="sidebar">
<aside class="sidebar">
    <div class="tablet-grid-30 grid-25 ">
        <section class="ferramentas hide-on-tablet hide-on-mobile">
            <div class="grid-container grid-parent">
                <h2>Ferramentas</h2>
                <div class="mobile-grid-33 tablet-grid-100 grid-parent grid-100">
                    <div class="box-ferramenta cf">
                        <a href="<?= get_home_url(); ?>/projecao-financeira">
                            <div class="tablet-grid-20 grid-parent grid-20">
                                <i class="icon-dinheiro"></i>
                            </div>
                            <div class="tablet-grid-80 grid-parent grid-80">
                                <span>Projeção de Previdência</span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="mobile-grid-33 tablet-grid-100 grid-parent grid-100">
                    <div class="box-ferramenta cf">
                        <a href="<?= get_home_url(); ?>/ferramenta-comparador-de-previdencia/">
                            <div class="tablet-grid-20 grid-parent grid-20">
                                <i class="icon-dinheiro"></i>
                            </div>
                            <div class="tablet-grid-80 grid-parent grid-80">
                                <span>Comparador de Previdência</span>
                            </div>

                        </a>
                    </div>
                </div>
                <div class="mobile-grid-33 tablet-grid-100 grid-parent grid-100">
                    <div class="box-ferramenta cf">
                        <a href="<?= get_home_url(); ?>/ferramenta-de-simulador-simples/">
                            <div class="tablet-grid-20 grid-parent grid-20">
                                <i class="icon-dinheiro"></i>
                            </div>
                            <div class="tablet-grid-80 grid-parent grid-80">
                                <span>Simulador Simples</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="branch branch-plano hide-on-tablet hide-on-mobile">
            <?php
                $banneri = new WP_Query( 
                    array(

                        'post_type'      => 'bannersInvestimento'
                    )
                );

                while ( $banneri->have_posts() ) : $banneri->the_post(); 

                    $imageBanneri = get_field('imagem');
                    $size = 'full';
                    if( !empty($imageBanneri) ): 

                ?>
            <a href="<?= get_site_url(); ?>/contato">
                   
                <img src="<?php echo $imageBanneri['url']; ?>" alt="" />
            </a>

            <?php endif; ?>
            <?php endwhile; ?>

        </section>
        <section class="social hide-on-mobile ">
            <div class="grid-container">
                <h2>Redes Sociais</h2>
                <ul>
                    <li>
                        <a href="https://www.facebook.com/WeInvestDigital" target="_blank"> <i class="icon-facebook"></i> CURTIR</a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/weinvest_" target="_blank"> <i class="icon-instagram"></i> SEGUIR</a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCc1B5e3fFJhCAp5BQFmRC4g" target="_blank"> <i class="icon-youtube-play"></i> INSCREVER</a>
                    </li>
                    <li>
                        <a href="https://twitter.com/WeInvestDigital" target="_blank"> <i class="icon-twitter"></i> SEGUIR </a>
                    </li>
                </ul>

            </div>
        </section>

        <section class="cats cat-single cf cat-aside ">
                <?php if ( is_home()) {?>
                <div class="grid-container">
                    <div class="s-cat cf">
                        <div class="mobile-grid-100 grid-parent">
                            <h2>Posts populares:</h2>
                        </div>
                    </div>
                </div>
                <div class="single-post-cat cf posts posts-populares">
                    <?php // WP_Query arguments
                        $args2 = array(
                            'posts_per_page'         => '4',
                            'meta_query'	=> array(
                                array(
                                    'key'	 	=> 'post_popular',
                                    'compare' 	=> '=',
                                    'value'	  	=> true
                                )
                            ),
                        );

                        // The Query
                        $vitrine2 = new WP_Query( $args2 ); 

                        if ( $vitrine2->have_posts() ) : 
                            while ( $vitrine2->have_posts() ) : $vitrine2->the_post(); 

                            $category = get_the_category();
                            $catParent = $category[0]->parent;

                            $catName = "";
                            $catSlug = "";

                            if( $catParent > 0){
                                $catPai = get_category( $catParent );
                                $catId = $catPai->cat_ID;
                                
                                $catName = $catPai->name;
                                $catSlug = $catPai->slug;
                            }else{
                                $catId   = $category[0]->cat_ID;
                                $catName = $category->name;
                                $catSlug = $category->slug;
                            } ?>

                        <div>
                            <div class="box-posts setCookie" data-cat=" <?= $catId; ?>">
                                <a href="<?php the_permalink(); ?>" title="">
                                    <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>)">

                                        <figcaption itemprop="caption description" class="caption">
                                            <span class="post-cat <?= $catSlug; ?>">
                                                <?= $catName; ?>
                                            </span>
                                        </figcaption>
                                    </figure>

                                    <h2 class="<?= $catSlug; ?>">
                                        <?= title_limite(40); ?>
                                    </h2>
                                    <span class="data none">
                                        <?= get_the_date(); ?></span>
                                    <p>
                                        <?php wp_limit_post(150,'...',true);?>
                                    </p>
                                </a>
                            </div>
                        </div>
                    
                    <?php endwhile; 
                    wp_reset_query();
                    endif; ?>
                </div>
                <?php } ?>
        </section>

        <section class="branch branch-cambio hide-on-mobile">

            <div class="grid-container grid-parent">
                <?php
                    $bannercr = new WP_Query( 
                        array(

                            'post_type'      => 'bannersCriptomoeda'
                        )
                    );

                    while ( $bannercr->have_posts() ) : $bannercr->the_post(); 

                        $imageBannercr = get_field('imagem');
                        $size = 'full';
                        if( !empty($imageBannercr) ): 

                        ?>
                <a href="<?= get_field('link') ?>">
                    <img src="<?php echo $imageBannercr['url']; ?>" alt="" />
                </a>

                <?php endif; ?>
                <?php endwhile; ?>

            </div>

        </section>

        
        <section class="branch">
            <div class="grid-container grid-parent">
                <?php
                    $bannerc = new WP_Query( 
                        array(

                            'post_type'      => 'bannersCambio'
                        )
                    );

                    while ( $bannerc->have_posts() ) : $bannerc->the_post(); 

                        $imageBannerc = get_field('imagem');
                        $size = 'full';
                        if( !empty($imageBannerc) ): 

                    ?>
                <a href="<?= get_field('link') ?>">
                    <img src="<?php echo $imageBannerc['url']; ?>" alt="" />
                </a>
                <?php endif; ?>
                <?php endwhile; ?>
            </div>

        </section>

        
    </div>
</aside>
