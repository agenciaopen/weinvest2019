<?php

get_header();
$url = "http://".$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

?>
<div class="page-interna pg-single single cf">
    <div class="grid-container">

        <div class="tablet-grid-60 tablet-prefix-20 tablet-suffix-20 grid-70 prefix-15 suffix-30 hide-on-desktop">
            <section class="box-banner cf">
                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                    <section class="banner cf">
                        <?php
                                    $bannerm = new WP_Query(
                                        array(
                                            'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();

                                        $imageBannerm = get_field('imagem-desk');
                                        $size = 'full';
                                        if (!empty($imageBannerm)) : ?>
                        <a href="<?= get_field('link'); ?>">
                            <img src="<?= $imageBannerm['url']; ?>" alt="" />
                        </a>
                        <?php endif; ?>
                        <?php endwhile; ?>
                    </section>
                </div>

                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                    <section class="banner cf">
                        <?php
                                    $bannerm = new WP_Query(
                                        array(
                                          'post_type'      => 'banners',
                                            'posts_per_page' => 1
                                        )
                                    );

                                    while ($bannerm->have_posts()) : $bannerm->the_post();
                                        $imageBannerm = get_field('imagem_celular');
                                        $size = 'full';

                                        if (!empty($imageBannerm)) : ?>
                        <a href="<?= get_field('link'); ?>">
                            <img src="<?= $imageBannerm['url']; ?>" alt="" />
                        </a>
                        <?php endif; ?>
                        <?php endwhile; ?>
                    </section>
                </div>
            </section>
        </div>

        <div class="tablet-grid-70 grid-70 suffix-5 grid-parent">

            <!-- breadcrumbs -->
            <div class="grid-container">
                <?php
                    if (function_exists('yoast_breadcrumb')) {
                        yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                    }
                ?>
            </div>

            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); 
                    $idCat = $_COOKIE['categoria'];
                    $cat = get_category($idCat);
                    $catName = $cat->name;
                    $catSlug = $cat->slug;
                    $catLink = get_category_link( $idCat );

                ?>
            <section class="single_content cf ">
                <div class="name-cat">
                    <div class="grid-container">
                        <a href="<?= $catLink; ?>" class="post-cat <?= $catSlug; ?>">
                            <?= $catName; ?>
                        </a>
                        <hr class="<?= $catSlug; ?>" />
                    </div>
                </div>
                <div class="box-list-cats cf">

                    <div class="mobile-grid-100 tablet-grid-100 grid-100">
                        <h1 class="title <?= $catSlug; ?>">
                            <?= get_the_title(); ?>
                        </h1>
                    </div>

                    <div class="mobile-grid-100 tablet-grid-100 grid-100">
                        <div class="social top">
                            <div class="grid-container">
                                <ul class="list-inline">
                                    <li>
                                        <p> COMPARTILHE:</p>
                                    </li>
                                    <li>
                                        <a href="http://facebook.com/share.php?u=<?= $url; ?>&title=<?= get_the_title(); ?>" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                            <i class="icon-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://twitter.com/intent/tweet?url=<?= $url; ?>&text=<?= get_the_title(); ?>&via=WeInvestNet" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Twitter" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                            <i class="icon-twitter"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="https://api.whatsapp.com/send?text=<?= get_the_title(); ?>: <?= $url; ?>" class="link-face" title="Compartilhar <?= get_the_title(); ?> no whatsapp">
                                            <i class="icon-whatsapp"></i>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <div class="grid-100 tablet-grid-100 mobile-grid-100">
                        <?php
                                if (has_post_thumbnail()) { ?>
                        <?php the_post_thumbnail('full'); ?>
                        <?php } ?>
                        <br>
                        <br>
                    </div>

                    <div class="content-txt">
                        <p>
                            <?php the_content(); ?>
                        </p>
                    </div>
                </div>
            </section>

            <section class="user-post cf">
                <div class="grid-container">
                    <hr />
                    <div class="mobile-grid-35 tablet-grid-20 grid-20">
                        <?= get_avatar(get_the_author_meta('ID'), 200); ?>
                    </div>
                    <div class="mobile-grid-65 tablet-grid-75 grid-75">
                        <h3>
                            <?= get_author_name(); ?>
                        </h3>
                        <p>
                            <?= get_the_author_meta('description'); ?>
                        </p>
                    </div>
                    <hr />
                </div>
            </section>

            <section class="shared cf">
                <div class="grid-container">
                    <div class="social">
                        <div class="grid-container">
                            <ul>
                                <li>
                                    <p> COMPARTILHE:</p>
                                </li>
                                <li>
                                    <a href="http://facebook.com/share.php?u=<?= $url; ?>&title=<?= get_the_title(); ?>" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Facebook" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                        <i class="icon-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/intent/tweet?url=<?= $url; ?>&text=<?= get_the_title(); ?>&via=WeInvestNet" target="_blank" class="link-face" title="Compartilhar <?= get_the_title(); ?> no Twitter" onclick="window.open(this.href, 'tb-face-popup', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,left=380,top=370,width=400,height=200'); return false;">
                                        <i class="icon-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://api.whatsapp.com/send?text=<?= get_the_title(); ?>: <?= $url; ?>" class="link-face" title="Compartilhar <?= get_the_title(); ?> no whatsapp">
                                        <i class="icon-whatsapp"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </section>

            <section class="box-banner cf">
                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-mobile">
                    <section class="banner cf">
                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                                'post_type'      => 'banners',
                                                'offset' => 1,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();

                                            $imageBannerm = get_field('imagem-desk');
                                            $size = 'full';
                                            if (!empty($imageBannerm)) : ?>
                        <a href="<?= get_field('link'); ?>">
                            <img src="<?= $imageBannerm['url']; ?>" alt="" />
                        </a>
                        <?php endif; ?>
                        <?php endwhile; ?>
                    </section>
                </div>

                <div class="tablet-grid-90 tablet-prefix-5 tablet-suffix-5 grid-80 prefix-10 suffix-10 hide-on-tablet hide-on-desktop grid-parent">
                    <section class="banner cf">
                        <?php
                                        $bannerm = new WP_Query(
                                            array(
                                               'post_type'      => 'banners',
                                                'offset' => 1,
                                                'posts_per_page' => 1
                                            )
                                        );

                                        while ($bannerm->have_posts()) : $bannerm->the_post();
                                            $imageBannerm = get_field('imagem_celular');
                                            $size = 'full';

                                            if (!empty($imageBannerm)) : ?>
                        <a href="<?= get_field('link'); ?>">
                            <img src="<?= $imageBannerm['url']; ?>" alt="" />
                        </a>
                        <?php endif; ?>
                        <?php endwhile; ?>
                    </section>
                </div>
            </section>

            <section class="menu-posts cf">
                <div class="grid-container">
                    <div class="grid-100 grid-container">
                        <?php
                                    
                                    $categorias = get_categories(array(
                                        'orderby'  => 'name',
                                        'child_of' => $idCat
                                    ));
                                    
                                    ?>
                        <h2>Outros fundos de investimentos:</h2>
                        <ul>
                            <li>
                                <span class="align-center">FILTROS </span>
                            </li>
                            <?php foreach ($categorias as $categoria) { ?>
                            <li class="<?php if($categoria->cat_ID == $idCat ) echo " ativo"; ?>">
                                <a href="<?= get_category_link($categoria->cat_ID); ?>">
                                    <?= $categoria->cat_name; ?>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </section>

            <div class="comentarios">
            <div id="fb-root"></div>
                <script>(function(d, s, id){var js,fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s);js.id = id;js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-comments" data-href="<?php echo $ur; ?>" data-width="100%" data-numposts="5"></div>
            </div>
            <?php
            endwhile;
        endif;
        wp_reset_postdata();
        ?>
            <section class="cats cat-single cf ">
                <div class="grid-container">
                    <div class="s-cat cf">
                        <div class="mobile-grid-100 grid-parent">
                            <h2>Posts populares:</h2>
                        </div>

                    </div>
                </div>


                <div class="list-post-cat list-post-cat-single posts-populares">
                    <?php // WP_Query arguments
            $args2 = array(
                'posts_per_page'         => '4',
                'meta_query'    => array(
                    array(
                        'key'       => 'post_popular',
                        'compare'   => '=',
                        'value'     => true
                    )
                ),
            );

            // The Query
            $vitrine2 = new WP_Query( $args2 ); 

            if ( $vitrine2->have_posts() ) : 
                while ( $vitrine2->have_posts() ) : $vitrine2->the_post(); 
                    
                    $category = get_the_category();
                    $catParent = $category[0]->parent;

                    $catName = "";
                    $catSlug = "";

                    if( $catParent > 0){
                        $catPai = get_category( $catParent );
                        $catId = $catPai->cat_ID;
                        
                        $catName = $catPai->name;
                        $catSlug = $catPai->slug;
                    }else{
                        $catId   = $category[0]->cat_ID;
                        $catName = $category->name;
                        $catSlug = $category->slug;
                    }
                ?>
                    <div>
                        <div class="box-posts setCookie" data-cat="<?= $catId; ?>">
                            <a href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>">
                                <figure itemprop="" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url('<?= wp_get_attachment_url(get_post_thumbnail_id()); ?>)">
                                    <figcaption itemprop="caption description" class="caption">
                                        <span class="post-cat <?= $catSlug;?>">
                                            <?= $catName;?></span>
                                    </figcaption>
                                </figure>
                                <h2 class="<?= $catSlug; ?>">
                                    <?= title_limite(60); ?>
                                </h2>
                                <span class="data">
                                    <?= get_the_date(); ?></span>
                                <p>
                                    <?php wp_limit_post(150,'...',true);?>
                                </p>
                            </a>
                        </div>
                    </div>

                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>

            </section>

        </div>

        <!-- Sidebar  -->
        <?php get_sidebar(); ?>
        <!-- /Sidebar  -->
    </div>
</div>

<?php
get_footer(); ?>
