<footer class="footer cf">
    <div class="grid-container">
        <div class="tablet-grid-40 tablet-prefix-30 tablet-suffix-30 grid-30 prefix-35 suffix-35">
            <div class="logo-footer cf">
                <div class="mobile-grid-50 mobile-prefix-25 mobile-suffix-25 ">
                    <a href="">
                        <img src="<?php bloginfo('template_url')?>/build/imgs/logo.svg" alt="" />
                    </a>
                </div>
            </div>
        </div>

        <div class="tablet-grid-35 grid-25">
            <section class="social box-footer">
                <div class="grid-container">
                    <h2>Redes Sociais</h2>
                    <ul>
                        <li>
                            <a href=""> <i class="icon-facebook"></i> CURTIR</a>
                        </li>
                        <li>
                            <a href=""> <i class="icon-instagram"></i> SEGUIR</a>
                        </li>
                        <li>
                            <a href=""> <i class="icon-youtube-play"></i> INSCREVER</a>
                        </li>
                        <li>
                            <a href="https://twitter.com/WeInvestTweet" target="_blank"> <i class="icon-twitter"></i> SEGUIR</a>
                        </li>
                    </ul>

                </div>
            </section>
        </div>

        <div class="mobile-grid-100 grid-parent tablet-grid-55 tablet-prefix-5 grid-40 prefix-5">
            <ul class="links-footer cf">
                <div class="mobile-grid-parent tablet-grid-50 grid-50">
                    <li><a href="<?= get_site_url(); ?>"><i class="icon-right-open-mini"></i> <span> Home </span> </a></li>
                    <li><a href="<?= get_site_url(); ?>/categoria/como-investir/"><i class="icon-right-open-mini"></i> <span> Como Investir </span> </a>
                        <?php
                            $childCats = get_categories(array(
                                'orderby'  => 'name',
                                'child_of' => 2
                            ));
                            if(!empty($childCats)) : ?>
                                <ul class="submenu">
                                    <?php foreach ($childCats as $childcat) { ?>
                                        <li>
                                            <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                                <?= $childcat->cat_name; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                        <?php endif; ?>

                    </li>
                    <!-- <li><a href="<?= get_site_url(); ?>/categoria/mercado-e-noticias"><i class="icon-right-open-mini"></i> <span> Mercado e Notícias </span></a></li> -->
                    <li><a href="<?= get_site_url(); ?>/categoria/renda-fixa/"><i class="icon-right-open-mini"></i> <span> Renda Fixa </span> </a>
                        <?php
                            $childCats = get_categories(array(
                                'orderby'  => 'name',
                                'child_of' => 1
                            ));
                            if(!empty($childCats)) : ?>
                                <ul class="submenu">
                                    <?php foreach ($childCats as $childcat) { ?>
                                        <li>
                                            <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                                <?= $childcat->cat_name; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                        <?php endif; ?>
                    </li>
                    <li><a href="<?= get_site_url(); ?>/categoria/tesouro-direto"><i class="icon-right-open-mini"></i> <span> Tesouro Direto </span></a>
                        <?php
                            $childCats = get_categories(array(
                                'orderby'  => 'name',
                                'child_of' => 4
                            ));
                            if(!empty($childCats)) : ?>
                                <ul class="submenu">
                                    <?php foreach ($childCats as $childcat) { ?>
                                        <li>
                                            <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                                <?= $childcat->cat_name; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                        <?php endif; ?>
                    </li>
                </div>
                <div class="mobile-grid-parent tablet-grid-50 grid-50">

                    <li><a href="<?= get_site_url(); ?>/categoria/acoes"><i class="icon-right-open-mini"></i> <span> Ações </span> </a>
                        <?php
                            $childCats = get_categories(array(
                                'orderby'  => 'name',
                                'child_of' => 3
                            ));
                            if(!empty($childCats)) : ?>
                                <ul class="submenu">
                                    <?php foreach ($childCats as $childcat) { ?>
                                        <li>
                                            <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                                <?= $childcat->cat_name; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                        <?php endif; ?>
                    </li>
                    <li><a href="<?= get_site_url(); ?>/categoria/como-investir/fundos-de-investimentos"><i class="icon-right-open-mini"></i> <span> Fundos de Investimentos </span> </a>
                        <?php
                            $childCats = get_categories(array(
                                'orderby'  => 'name',
                                'child_of' => 5
                            ));
                            if(!empty($childCats)) : ?>
                                <ul class="submenu">
                                    <?php foreach ($childCats as $childcat) { ?>
                                        <li>
                                            <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                                <?= $childcat->cat_name; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                        <?php endif; ?>
                    </li>

                    <!-- <li><a href="<?= get_site_url(); ?>/categoria/planejamento-financeiro"><i class="icon-right-open-mini"></i> <span> Planejamento Financeiro </span> </a>
                        <?php
                            $childCats = get_categories(array(
                                'orderby'  => 'name',
                                'child_of' => 7
                            ));
                            if(!empty($childCats)) : ?>
                                <ul class="submenu">
                                    <?php foreach ($childCats as $childcat) { ?>
                                        <li>
                                            <a href="<?= get_category_link($childcat->cat_ID); ?>">
                                                <?= $childcat->cat_name; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                        <?php endif; ?>
                    </li> -->
                </div>

            </ul>
        </div>

        <div class="tablet-grid-40 tablet-prefix-5 grid-25 prefix-5">
            <div class="form cf">
                <h2>Assine nossa newsletter</h2>
                <label for="Nome">Nome:</label>
                <input type="text" id="User" name="Nome" />
                <label for="Nome">E-mail:</label>
                <input type="email" id="E-mail" name="E-mail" />
                <input type="submit" vale="Enviar" class="btn" />
            </div>
        </div>
    </div>

    <div class="tablet-grid-50">
        <div class="grid-container">
            <div class="txt-footer">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut </p>
            </div>
        </div>
    </div>

    <div class="menu-footer cf hide-on-tablet hide-on-desktop">
        <div class="mobile-grid-25 grid-parent">
            <a href="<?= get_home_url(); ?>/projecao-financeira">
                <i class="icon-dinheiro"></i>
                <span>Projeção de Previdencia</span>
            </a>
        </div>
        <div class="mobile-grid-30 grid-parent">
            <a href="<?= get_home_url(); ?>/ferramenta-comparador-de-previdencia/">
                <i class="icon-dinheiro"></i>
                <span>Comparador de previdência</span>
            </a>
        </div>
        <div class="mobile-grid-25 grid-parent">
            <a href="<?= get_home_url(); ?>/ferramenta-de-simulador-simples/">
                <i class="icon-dinheiro"></i>
                <span>Simulador <br />Simples</span>
            </a>
        </div>
        <div class="mobile-grid-20 grid-parent">
            <a href="<?= get_home_url();?>/contato">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    width="47.926px" height="47.926px" viewBox="0 0 47.926 47.926" style="enable-background:new 0 0 47.926 47.926;"
                    xml:space="preserve">
                    <g>
                        <g>
                            <g>
                                <path d="M39.928,0.09H8c-4.4,0-8,3.6-8,8v20.977c0,4.4,3.6,8,8,8h7.397v10.77l13.333-10.77h11.195c4.399,0,8-3.6,8-8V8.09
                                    C47.928,3.69,44.328,0.09,39.928,0.09z M42.928,29.066c0,1.627-1.374,3-3,3H28.73h-1.768l-1.375,1.11l-5.191,4.193v-0.305v-5h-5
                                    H8c-1.626,0-3-1.373-3-3V8.09c0-1.626,1.374-3,3-3h31.928c1.626,0,3,1.374,3,3V29.066L42.928,29.066z"/>
                                <circle cx="13.707" cy="18.58" r="2.913"/>
                                <circle cx="23.964" cy="18.58" r="2.913"/>
                                <circle cx="34.22" cy="18.58" r="2.913"/>
                            </g>
                        </g>
                    </g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    
                </svg>
            </a>
        </div>

    </div>
</footer>

<?php wp_footer(); ?>

<script src="<?php bloginfo('template_url')?>/build/js/generico-lib.min.js"></script>
<script src="<?php bloginfo('template_url')?>/build/js/scripts.min.js"></script>

<script src="<?php bloginfo('template_url') ?>/build/js/js.cookie.js"></script>

<?php

    if ( is_front_page() && is_home() ) { ?>
<script src="<?php bloginfo('template_url')?>/build/js/home-lib.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url')?>/build/css/home-lib.min.css">

<?php } else { ?>
    <script src="<?php bloginfo('template_url')?>/build/js/interna-lib.min.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url')?>/build/css/interna-lib.min.css">
<?php  }  ?>
